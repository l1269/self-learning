import React, { useState } from "react";
import { useAuth } from "../../hooks/useAuth";
import { Link } from "react-router-dom";
import { collection, addDoc, getDocs } from "firebase/firestore";
import { db } from "../../firebase/config";
import { useNavigate } from "react-router-dom";
import "./Signup.css";
import { FiUser } from "react-icons/fi";
import { HiOutlineMail } from "react-icons/hi";
import { BiLockAlt } from "react-icons/bi";
import logo from "../../assets/design/logo2.png";
import { useEffect } from "react";

function Signup() {
  const navigate = useNavigate();

  const { signup } = useAuth();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");
  const [auth, setAuth] = useState([]);
  const [errorAdmin, setErrorAdmin] = useState("");
  let admin = null;

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  useEffect(() => {
    const getAuth = async () => {
      const querySnapshot = await getDocs(collection(db, "auth"));
      querySnapshot.forEach((doc) => {
        setAuth(
          querySnapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }))
        );
      });
    };
    getAuth();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    // vérifie si l'utilisateur est connecté à internet et renvoie un boolean
    if (!navigator.onLine) {
      return setError("Vérifier votre connexion internet");
    }
    // console.log(email, password, confirmPassword, firstName, lastName)
    if (
      firstName === "" ||
      lastName === "" ||
      email === "" ||
      password === "" ||
      confirmPassword === ""
    ) {
      // document.querySelectorAll('input').style.border = '2px solid red'
      return setError("Veuillez remplir les champs vides");
    }
    if (password.length < 6) {
      return setError("Mot de passe court (au moins 6 caracteres)");
    }
    if (password !== confirmPassword) {
      return setError("Les mots de passe doivent correspondre !");
    }

    try {
      setLoading(true);
      setError("");
      auth.forEach((user) => {
        if (user.status === "admin") {
          return (admin = true);
        }
      });

      if (admin) {
        setLoading(false);
        return setErrorAdmin(`Impossible d'avoir un autre admin !`);
      } else {
        await signup(email, password);
        await addDoc(collection(db, "auth"), {
          prenom: firstName,
          nom: lastName,
          email,
          status: "admin",
        });

        setSuccess("Inscription reussie !");

        setTimeout(() => {
          navigate("/");
        }, 1500);
      }
    } catch (err) {
      setLoading(false);
      if (err.code === "auth/email-already-in-use") {
        return setError(
          `L'adresse e-mail fournie est déjà utilisée par un utilisateur existant !`
        );
      }
      if (err.code === "auth/invalid-email") {
        return setError(
          `La valeur fournie pour la propriété d'utilisateur de email n'est pas valide !`
        );
      }
    }
    setLoading(false);
  };

  return (
    <div style={{ overflow: "hidden" }} className="vh-150">
      {/* {
        auth && auth.map(user => (
          <p>{user.status}</p>
        ))
      } */}
      <div className="logo text-center d-flex justify-content-center mb-3 mt-4">
        <img
          className="me-1"
          style={{ maxWidth: "40px" }}
          src={logo}
          alt="Logo"
        />
        <h3 className="align-self-end mb-1" style={{ fontWeight: "100" }}>
          Learning
        </h3>
      </div>
      <p style={{ fontWeight: "100" }} className="text-center">
        Le meilleur endroit pour se former !
      </p>
      <div
        style={{ maxWidth: "450px" }}
        className="container w-sm-75 mx-sm-auto shadow p-3 mb-5"
      >
        <h1 className="text-center mb-4">Inscription</h1>
        {errorAdmin && (
          <div className="bg-danger px-4 py-2 text-light text-center mb-3">
            {errorAdmin}
          </div>
        )}
        {error && (
          <div className="bg-danger px-4 py-2 text-light text-center mb-3">
            {error}
          </div>
        )}
        {success && (
          <div className="bg-success px-4 py-2 text-light text-center mb-3">
            {success}
          </div>
        )}
        <form className="pb-3 w-100" autoComplete="off" onSubmit={handleSubmit}>
          <div className="mb-3 position-relative">
            <FiUser className="subs-icon" />
            <input
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
              type="text"
              className="login-input"
              id="firstName"
              aria-describedby="firstname"
              placeholder="Prenom"
            />
          </div>
          <div className="mb-3 position-relative">
            <FiUser className="subs-icon" />
            <input
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
              type="text"
              className="login-input"
              id="lastName"
              aria-describedby="lastname"
              placeholder="Nom"
            />
          </div>
          <div className="mb-3 position-relative">
            <HiOutlineMail className="subs-icon" />
            <input
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              type="email"
              className="login-input"
              id="email"
              aria-describedby="emailHelp"
              placeholder="Adresse Email"
            />
          </div>
          <div className="mb-3 position-relative">
            <BiLockAlt className="subs-icon" />
            <input
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              type="password"
              className="login-input"
              id="password"
              placeholder="Mot de passe"
            />
          </div>
          <div className="mb-3 position-relative">
            <BiLockAlt className="subs-icon" />
            <input
              value={confirmPassword}
              onChange={(e) => setConfirmPassword(e.target.value)}
              type="password"
              className="login-input"
              id="confirmPassword"
              placeholder="Confirmer mot de passe"
            />
          </div>
          <button
            disabled={loading}
            type="submit"
            className="btn py-2 auth w-100"
          >
            {loading ? "inscription en cours..." : "Inscrivez-vous"}
          </button>
        </form>
        {errorAdmin && (
          <Link className="inline-block text-center" to="/">
            <u>Veuillez vous connectez</u>
          </Link>
        )}
      </div>
    </div>
  );
}

export default Signup;
