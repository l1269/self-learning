import "./sideBar.css";
import logo from "../../assets/design/logo2.png";
import NavLinke from "./NavLinke";

function SideBar({ links }) {
  return (
    <nav id="sidebar" style={{position: 'fixed', zIndex: '1', backgroundColor: 'white', maxWidth: '230px'}} className="Sidebar vh-100 p-1 p-md-3 d-sm-block d-none">
      {/* logo */}
      <div className="logo p-lg-2 p-0">
        <div className="logo text-center text-lg-start mb-3 mt-0">
          <img
            className="me-lg-1 logo-img"
            style={{ maxWidth: "40px" }}
            src={logo}
            alt="Logo"
          />
          <h3
            className=" mb-1 d-lg-inline d-none"
            style={{ fontWeight: "100", display: "inline" }}
          >
            Learning
          </h3>
        </div>
      </div>
      {/* menu */}
      <ul className="p-0">
        {links.map((link, key) => (
          <NavLinke
            key={key}
            label={link.label}
            Icon={link.icon}
            to={link.to}
          />
        ))}


        {/* <li className='nav-item position-relative'>
                </li> */}
      </ul>
    </nav>
  );
}

export default SideBar;
