import { collection, onSnapshot, query } from 'firebase/firestore';
import React, { useEffect, useState } from 'react'
import { db } from '../../../firebase/config';

function GetProf({ id }) {

    const [profs, setProfs] = useState([])

    useEffect(() => {

        const getProfs = async () => {
            const q = query(collection(db, "professeur"));
            onSnapshot(q, (querySnapshot) => {

                const usersInfo = [];
                querySnapshot.forEach((doc) => {
                    usersInfo.push({ ...doc.data(), id: doc.id });
                    setProfs([...usersInfo]);

                });
            });

        }
        getProfs();
    }, [])

    const prof = profs?.filter(prof => prof.id === id)[0]

    return (
        <h4 style={{ textTransform: 'uppercase', marginLeft: 40 }}>Coach : {
            !prof ? <div className='loading inline-block'></div> : prof?.prenom + ' ' + prof?.nom
        }</h4>
    )
}

export default GetProf