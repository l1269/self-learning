
import "./FiltreCours.css";

const FiltreCours = ({ cours, setListCours }) => {
  const filterDomain = (domainName) => {
    let domain = domainName;
    if (domain === "domain") {
      return setListCours(cours);
    }
    setListCours(cours.filter((cour) => cour.domaine === domain));
  };

  return (
    <div className="form-container"
      style={{
        boxShadow: 'rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px',
        marginBottom: 15,
      }}
    >
      {/* <form className="mt-2 mb-4 mx-0 p-3 filter-form"> */}
      <select
        id="selected"
        onChange={(e) => filterDomain(e.target.value)}
        className="btn-select"
        aria-label="Default select example"
      >
        <option value="domain">Tous les domaines</option>
        <option value="programmation">Programmation</option>
        <option value="design">Design</option>
        <option value="marketing">Marketing</option>
        <option value="entrepreneuriat">Entrepreneuriat </option>
      </select>
      {/* </form> */}
    </div>
  );
};

export default FiltreCours;
