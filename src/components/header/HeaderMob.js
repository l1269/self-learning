import React, { useState } from "react";
import { AiOutlineLogout } from "react-icons/ai";
import { BsPersonCircle, BsSearch } from "react-icons/bs";
import { useAuth } from "../../hooks/useAuth";
import { useNavigate } from "react-router-dom";
import { collection, getDocs, query, where } from "firebase/firestore";
import { db } from "../../firebase/config";
import { useEffect } from "react";
import logo from "../../assets/design/logo2.png";
import "./Header.css";

const styles = {
  searchContainer: {
    width: '100%',
    height: 35,
    backgroundColor: 'white',
    borderRadius: 7,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '0 20px',
    // marginRight: 20,
    boxShadow: 'rgba(0, 0, 0, 0.1) 0px 5px 10px -5px, rgba(0, 0, 0, 0.04) 0px 5px 5px -5px',
  },

  input: {
    all: 'unset',
    width: '100%',
    height: '100%',
  },

  btnSearch: {
    all: 'unset',
    height: 40,
    padding: '0 15px',
    borderRadius: '7px',
    backgroundColor: '#7a89fe',
    color: 'white',
    fontSize: 15,
    fontWeight: 400,
    boxShadow: 'rgba(0, 0, 0, 0.1) 0px 20px 25px -5px, rgba(0, 0, 0, 0.04) 0px 10px 10px -5px',
    cursor: 'pointer'
  }

}

const HeaderMob = ({ title, page, handleSearch }) => {
  const navigate = useNavigate();
  const { logout, currentUser } = useAuth();
  const [auth, setAuth] = useState([]);

  useEffect(() => {
    const getAuth = async () => {
      const q = query(
        collection(db, "auth"),
        where("email", "==", currentUser.email)
      );
      const querySnapshot = await getDocs(q);
      querySnapshot.forEach((doc) => {
        setAuth(
          querySnapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }))
        );
      });
    };
    getAuth();
  }, []);

  const signout = async () => {
    try {
      await logout();
      navigate("/");
    } catch {
      alert(`Une erreur s'est produite !`);
    }
  };

  return (
    <header
      id="headerMob"
      style={{ padding: "1rem" }}
      className="text-dark px-3 w-100 border-bottom d-none d-sm-block sticky-top"
    >
      <div className="d-flex justify-content-between align-items-center">
        <h2 className="m-0 fixed-h2">
          {title ? (
            title
          ) : (
            <div style={{ display: "flex", alignItems: "center" }}>
              <img
                className="me-lg-1 logo-img  d-inline"
                style={{ maxWidth: "40px", marginTop: "-25px" }}
                src={logo}
                alt="Logo"
              />
              <h3 className="" style={{ fontWeight: 400, marginTop: 12 }}>
                Learning
              </h3>
            </div>
          )}
        </h2>

        {/* Search bar */}
        {
          page === 'apprenant' && (
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                margin: "0 20px",
                border: '1px solid gray',
                width: 500,
                borderRadius: 7
              }}
            >

              <div style={styles.searchContainer}>

                <input
                  style={styles.input}
                  placeholder="Recherche..."
                  onChange={handleSearch}
                />
                <BsSearch size={20} color={'grey'} />
              </div>
            </div>
          )
        }

        <div
          // style={{
          //   position: "fixed",
          //   right: "1.5%",
          // }}
          className="user-infos"
        >
          <div className="dropdown">
            <button
              className="dropdown-toggle drop"
              type="button"
              id="dropdown"
              data-bs-toggle="dropdown"
              aria-expanded="false"
            >
              <BsPersonCircle style={{ fontSize: "2rem" }} />{" "}
              {auth.length !== 0
                ? `${auth[0]?.prenom} ${auth[0]?.nom}`
                : "loading..."}
            </button>
            <ul className="dropdown-menu py-0 me-0" aria-labelledby="dropdown">
              <li className="text-center bg-danger opacity-95 rounded py-2 px-0">
                <button
                  style={{ border: "none", backgroundColor: "transparent" }}
                  onClick={signout}
                  className="p-0 text-light"
                >
                  <AiOutlineLogout style={{ fontSize: "1.2rem" }} /> Deconnexion
                </button>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </header>
  );
};

export default HeaderMob;
