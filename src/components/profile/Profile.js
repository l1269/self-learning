import React from 'react'

function Profile({ status, nom, prenom }) {
    return (
        <div
            style={{
                display: 'flex',
                alignItems: 'center',
            }}
        >
            <div
                style={{
                    borderRadius: '50%',
                    overflow: 'hidden',
                    marginRight: 10
                }}
            >
                <img
                    style={{
                        width: '50px',
                        height: '50px',
                        objectFit: 'cover'
                    }}
                    src={
                        status === 'prof' ? 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQuIbv-7JSgC23hcGq8qDRBpFzdMBEw8urHdQ&usqp=CAU' : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRW6X2lldt_gy2tcbXCKBbKWNVBpH-f1Mcjsw&usqp=CAU'
                    }

                    alt={status === 'prof' ? 'professeur' : 'apprenant'}
                />
            </div>

            <span
                style={{
                    fontWeight: 500,
                }}
            >
                {nom}
            </span>
        </div>
    )
}

export default Profile