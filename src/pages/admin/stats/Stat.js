import React from 'react'

function Stat({ title, Icon, number }) {

    const max = '100%'
    const styles = {
        container: {
            width: 200,
            height: 'auto',
            backgroundColor: '#fff',
            borderRadius: 7,
            padding: 10,
            marginRight: 40
        },

        title: {
            width: max,
            textAlign: 'right',
            fontSize: 15,
            fontWeight: 500,
            textTransform: 'uppercase',
            marginBottom: -15
        },

        info: {
            marginTop: 10,
            fontSize: 30,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
        },

        number: {
            fontSize: 18,
            fontWeight: 500
        }
    }

    return (
        <div className='Container-course progra'>
            <p style={styles.title}>{title}</p>
            <hr />
            <div style={styles.info}>
                <Icon />
                <span style={styles.number}>{number}</span>
            </div>
        </div>
    )
}

export default Stat