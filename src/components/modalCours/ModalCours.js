import './ModalCours.css'
import { db } from '../../firebase/config';

import { FiDelete } from 'react-icons/fi';



export default function ModalCours({ children, savedCour, onClose }) {

    // Notification
    

    return (
        <>
            <div className="overLay">
                
                <div className='modalContainer' style={{ maxHeight: 500, overflowY: 'auto'}}>
                    <p className='closeBtn' onClick={() => onClose(false)}><FiDelete size={30} /></p>
                    <div className='modalRight'>
                        <div className='content'>
                            <h1 className='titreModal mt-2'>
                                <span className='detailTitle'>Titre : </span>
                                <span className='detailValue'>{savedCour?.titre}</span>
                            </h1>
                            <h3 className='titreDomaine'>
                                <span className='detailTitle'>Domaine : </span>
                                <span className='detailValue'>{savedCour?.domaine}</span>
                            </h3>
                            <p>
                                <span className='detailTitle'>Date : </span>
                                <span className='detailValue'>{savedCour?.date}</span>
                            </p>
                            <p>
                                <span className='detailTitle'>Description : {' '}</span>
                                <span className='detailValue'>
                                    {savedCour?.description}
                                </span>
                            </p>
                            <div className='container-bouton'>
                                {/* <ModalEditCours cour={savedCour?} openModal={showModal} onOpenModal={handleOpenModal} onCloseModal={() => setShowModal(false)} /> */}
                            {
                                children
                            }
                            </div>
                            <p className='mt-4'>Ajouté par : {savedCour?.auteur}</p>
                        </div>

                    </div>
                </div>
            </div>

        </>
    )
}
