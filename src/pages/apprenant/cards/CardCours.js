import React from 'react'
import './CardCours.css'

export const CardCours = ({ img, titre, description, children, url }) => {
    return (
        <div
            style={{
                width: '100%',
                minHeight: 209,
                backgroundColor: '#ffff',
                padding: '15px',
                borderRadius: '10px',
                marginBottom: 20,
                boxShadow: 'rgba(0, 0, 0, 0.09) 0px 3px 12px',
            }}
        >
            <span
                style={{
                    float: 'left',
                    width: 250,
                    height: 180,
                    borderRadius: 10,
                    overflow: 'hidden',
                    margin: '0 15px 5px 0',
                }}
            >

                <img
                    src={img}
                    alt="cours"
                    style={{
                        width: '100%',
                        height: '100%',
                        objectFit: 'cover'
                    }}
                />
            </span>
            <div
                style={{
                    width: '100%',
                    height: '100%',
                    paddingLeft: 10
                }}
            >
                <h4>{titre}</h4>

                <p style={{ fontSize: 13 }}>{description}</p>

                {/* <a 
                    href={url}
                    target="blank"
                    id='course-link'
                    
                >Lien</a> */}
                {children}
            </div>
        </div>
    )
}
