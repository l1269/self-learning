import React, {useEffect, useState} from 'react'
import './NombreApprenant.css'
import { db } from '../../../firebase/config'
import { collection, onSnapshot } from 'firebase/firestore'
import { TbUsers } from 'react-icons/tb'

export default function NombreApprenant() {

    const [nombre, setNombre]= useState()

    useEffect(()=>{
        const getProfs = ()=>{
            onSnapshot(collection(db, 'apprenant'), (querySnapshot)=>{
                const numberApprenant = querySnapshot.size
                setNombre(numberApprenant)
            })
        }
        getProfs()
    },[])

    const styles = {
        container: {
            width: '100%',
            height: 'auto',
            borderRadius: 7,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
            backgroundColor: '#fff',
            borderLeft: '7px solid #00171F',
            marginTop: 20,
            color: 'gray',
            padding: 7,
            boxShadow: 'rgba(149, 157, 165, 0.2) 0px 8px 24px'
        },

        title: {
            fontSize: 20,
            fontWeight: 400,
            textTransform: 'uppercase',
        },


        number: {
            textAlign: 'right',
            width: '100%',
            fontSize: 20,
            fontWeight: 500,
        },
    }

    return (
        <>
            <div style={styles.container}>
                <TbUsers size={60}/>
                <div style={{width: '100%', textAlign: 'right'}}>
                    <p style={styles.title}>Apprenants</p>
                    <span style={styles.number}>{nombre}</span>
                </div>
            </div>
        </>
  )
}
