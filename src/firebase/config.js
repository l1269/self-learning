import { initializeApp } from "firebase/app";
import {getAuth} from 'firebase/auth';
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";
import {getFunctions} from 'firebase/functions'

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCt4jyM_449UrowUTd6H4S-9FQMNRv_Vwo",
  authDomain: "self-learning-dev-96a49.firebaseapp.com",
  projectId: "self-learning-dev-96a49",
  storageBucket: "self-learning-dev-96a49.appspot.com",
  messagingSenderId: "517169517812",
  appId: "1:517169517812:web:5f1afb82d6834a200a3aa1"
};
// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
export const auth = getAuth(app)
export const storage = getStorage(app);
export const functions = getFunctions(app);
