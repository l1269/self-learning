import React from 'react'

function CardHomeApprenant({ title, Icon, description }) {

    const styles = {
        card: {
            backgroundColor: 'white',
            width: 260,
            height: 290,
            borderRadius: 10,
            padding: '20px',
            boxShadow: 'rgba(0, 0, 0, 0.2) 0px 18px 50px -10px',
            marginBottom: 40,
            marginLeft: 20
        }
    }

    return (
        <div style={styles.card}>
            <h3 style={{ color: 'grey', fontSize: 18 }}>{title}</h3>
            <Icon size={50} color={'grey'} />
            <p style={{ marginTop: 10, color: 'grey', fontSize: 13 }}>{description}</p>
        </div>
    )
}

export default CardHomeApprenant