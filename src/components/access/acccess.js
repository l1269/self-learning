import { collection, onSnapshot, query, where } from 'firebase/firestore'
import {db} from "../../firebase/config"




export function access(statut, email, navigate) {

     let profInfo = {};
     /*****************************************/
     //  recuperer dans firestore l'email et le statut de l'utilisateur actuel et le mettre dans profInfo
     const q = query(collection(db, "auth"), where("email", "==", `${email}`));
 
     onSnapshot(q, querySnapshot => {
 
       querySnapshot.forEach((doc) => {
         const {email, status} = doc.data();
         profInfo = {email, status};
       });
 
     })
     /*****************************************/
 
     /*
       vérifier si l'email du current user est egale à l'email de profInfo et
       verifier si le statut de profInfo est différent de professeur.
       si les deux sont vrais on retourne dans la page précédente
     */
     if(email === profInfo?.email && profInfo?.status !== statut) {
       navigate(-1);
     }
}

