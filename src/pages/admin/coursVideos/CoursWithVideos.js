import React, { useState } from 'react'
import './coursVideos.css'
import { v4 as uuidv4 } from 'uuid';
import { TbPlus, TbTrash } from 'react-icons/tb';
import TextEditor from '../../../components/textEditor/TextEditor';

function CoursWithVideos() {

    const [title, setTitle] = useState('')
    const [telephone, setTelephone] = useState('')
    const [domaine, setDomaine] = useState('')
    const [date, setDate] = useState('')
    const [description, setDescription] = useState('')
    const [videos, setVideos] = useState([
        { id: uuidv4(), description: '', src: '', title: '' },
    ]);

    const handleChangeInput = (id, event) => {
        const newInputFields = videos.map(i => {
            if (id === i.id) {
                i[event.target.name] = event.target.value
            }
            return i;
        })

        setVideos(newInputFields);
    }

    const handleAddFields = () => {
        setVideos([...videos, { id: uuidv4(), description: '', src: '', title: '' }])
    }

    const handleRemoveFields = id => {
        const values = [...videos];
        values.splice(values.findIndex(value => value.id === id), 1);
        setVideos(values);
    }


    const handleSubmit = (e) => {
        e.preventDefault();


        const data = {
            titre: title,
            telephone,
            domaine,
            date,
            description,
            videos,
        }

        console.log(data);
    }

    return (
        <div className="coursVideos">
            <div className="coursVideos-title"><h3>Add a new module</h3></div>

            <form>
                <div>
                    <div className="inputLabel">
                        <label>Title :</label>
                        <input placeholder='Title' type="text" className="input"
                            name="titre"
                            onChange={(e) => setTitle(e.target.value)}
                        />
                    </div>
                    <div className="inputLabel">
                        <label>Telephone :</label>
                        <input placeholder='Telephone' type="tel" className="input"
                            name="telephone"
                            onChange={(e) => setTelephone(e.target.value)}
                        />
                    </div>
                    <div className="inputLabel">
                        <label>Domaine :</label>
                        <input placeholder='Domaine' type="text" className="input"
                            name="domaine"
                            onChange={(e) => setDomaine(e.target.value)}
                        />
                    </div>
                    <div className="inputLabel">
                        <label>Date :</label>
                        <input placeholder='Date' type="date" className="input"
                            name="date"
                            onChange={(e) => setDate(e.target.value)}
                        />
                    </div>
                </div>
                <div className="module-description">
                    <label>Description du module : </label>
                    <textarea
                        name="description"
                        onChange={(e) => setDescription(e.target.value)}
                        placeholder='Entrer votre description ici...'
                        cols="30"
                        rows="7"
                    ></textarea>
                    {/* <TextEditor /> */}
                </div>
                <div className="videos">
                    <h4>Ajouter des videos</h4>
                    {
                        videos.map((input, index) => (
                            <div key={input.id}>
                                <span>Video {index + 1} : </span>
                                <div className='container'>
                                    <input type={"text"} placeholder={"Titre de la video"}
                                        value={input.title}
                                        name="title"
                                        onChange={event => handleChangeInput(input.id, event)}
                                    />
                                    <input type={"text"} placeholder={"URL de la video"}
                                        value={input.src}
                                        name="src"
                                        onChange={event => handleChangeInput(input.id, event)}
                                    />
                                    <textarea
                                        value={input.description}
                                        name="description"
                                        onChange={event => handleChangeInput(input.id, event)}
                                        cols="30"
                                        rows="3"
                                        style={{ marginBottom: '10px' }}
                                        placeholder={"description de la videos..."}
                                    ></textarea>
                                </div>
                                <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                                    <TbTrash color='red' size={20} style={{ marginRight: 7, cursor: 'pointer' }} onClick={() => handleRemoveFields(input.id)} />
                                </div>
                            </div>
                        ))
                    }
                    <div style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center', marginTop: 10 }}>
                        <span
                            className="btn-add"
                            onClick={handleAddFields}
                        >
                            <TbPlus size={18} style={{ marginRight: 10 }} />
                            ajouter video</span>
                    </div>
                </div>


                {/* Btn submit */}
                <div className='btn'>
                    <button type={"submit"} onClick={handleSubmit}>
                        Ajouter ce module
                    </button>
                </div>
            </form>
        </div>
    )
}

export default CoursWithVideos