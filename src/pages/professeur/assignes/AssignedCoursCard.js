import React from 'react'

function AssignedCoursCard({ titre, date, img }) {
    return (
        <>
            <div
                style={{
                    width: '100%',
                    backgroundColor: 'rgba(0,0,0, 0.06)',
                    height: 'auto',
                    borderRadius: 10,
                    padding: 10,
                    display: 'flex',
                    marginBottom: 15
                }}
            >
                <div
                    style={{
                        width: 50,
                        height: 50,
                        borderRadius: 10,
                        overflow: 'hidden',
                        marginRight: 7
                    }}
                >
                    <img src={`${img}`} alt={'cours'} width={'100%'} height={"100%"} style={{ objectFit: 'cover' }} />
                </div>
                <div>
                    <h4 style={{fontSize: 15}}>{titre}</h4>
                    <p style={{fontSize: 12}}>date: <strong>{`${date}`}</strong></p>
                </div>
            </div>

            <hr />
        </>
    )
}

export default AssignedCoursCard