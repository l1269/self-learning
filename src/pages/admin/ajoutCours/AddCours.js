import { useEffect, useState } from 'react'
import { addDoc, collection, serverTimestamp } from 'firebase/firestore';
import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import { db, storage } from '../../../firebase/config';
import './AjoutCours.css'
import { BsUpload } from 'react-icons/bs';
import { toast } from 'react-toastify';

function AddCours() {

    const [data, setData] = useState({})
    const [file, setFile] = useState('')
    const [per, setPerc] = useState(null);

    // Notification
    const notification = () => toast("Cours ajouter");

    useEffect(() => {
        const uploadFile = () => {
            const name = new Date().getTime() + file.name;

            const storageRef = ref(storage, name);
            const uploadTask = uploadBytesResumable(storageRef, file);

            uploadTask.on(
                "state_changed",
                (snapshot) => {
                    const progress =
                        (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                    console.log("Upload is " + progress + "% done");
                    setPerc(progress);
                    switch (snapshot.state) {
                        case "paused":
                            console.log("Upload is paused");
                            break;
                        case "running":
                            console.log("Upload is running");
                            break;
                        default:
                            break;
                    }
                },
                (error) => {
                    console.log(error);
                },
                () => {
                    getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
                        setData((prev) => ({ ...prev, img: downloadURL }));
                    });
                }
            );
        };
        file && uploadFile();
    }, [file]);

    // Get inputs values
    const handleChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;

        setData({ ...data, [name]: value });
    }

    // onSubmit the form
    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            console.log('abou');

            await addDoc(collection(db, 'cours'), {
                ...data,
                assigne: [],
                timeStamp: serverTimestamp(),
            });
            notification()

        } catch (error) {
            console.log(error);
        }
    }
    return (
        <div style={{ maxWidth: '600px' }} className="mx-auto p-0">
            <form style={{ width: '100%' }} onSubmit={handleSubmit} className='mx-sm-auto p-sm-4 p-2'>
                <h2 className='text-center py-4'>Ajouter Cours</h2>

                {/* Inpust */}

                <div className="mb-4">
                    <label htmlFor="file" style={{ textAlign: 'center', width: '100%' }}>
                        <span
                            style={{
                                backgroundColor: 'grey',
                                padding: 10,
                                borderRadius: 10,
                                cursor: 'pointer',
                                margin: 10,
                                color: 'white',
                            }}
                        >
                            <BsUpload style={{
                                margin: '0 auto'
                            }}
                                size={18}
                            />
                        </span>
                        {
                            file ? per === 100 ? `${file.name}` : 'loading...' : 'Choisir une image'
                        }
                    </label>
                    <input
                        type="file"
                        id="file"
                        onChange={(e) => setFile(e.target.files[0])}
                        style={{ display: "none" }}
                    />
                </div>
                <div className="mb-4">
                    <select
                        className='select'
                        // value={domaine}
                        onChange={handleChange}
                        name={'domaine'}
                        required
                    >
                        <option value="">Selectionnez le domaine</option>
                        <option value="programmation">Programmation</option>
                        <option value="design">Design</option>
                        <option value="marketing">Marketing Digital</option>
                        <option value="entrepreneuriat">entrepreneuriat</option>
                    </select>
                </div>

                <div className="mb-4">
                    <input
                        type="text"
                        className="input"
                        placeholder='Titre'
                        name={'titre'}
                        // value={titre}
                        onChange={handleChange}
                        required
                    />
                </div>

                <div className="mb-4">
                    <input
                        type="text"
                        className='input'
                        placeholder='Durée'
                        name={'duree'}
                        // value={duree}
                        onChange={handleChange}
                        required
                    />
                </div>

                <div className="mb-4">
                    <input
                        type="text"
                        className='input'
                        placeholder='Votre nom'
                        // value={nomAjouteur}
                        name={'nomAjouteur'}
                        onChange={handleChange}
                        required
                    />
                </div>

                <div className="mb-4">
                    <textarea
                        cols="30"
                        rows="5"
                        name={'description'}
                        // value={description}
                        onChange={handleChange}
                    ></textarea>
                </div>

                {/* Boutton submit */}
                <button className='btn-ajouter fs-5 px-4 mb-4' type='submit'>Ajouter</button>
            </form>
        </div>
    )
}

export default AddCours