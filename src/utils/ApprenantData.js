import { Fa500Px, FaAccusoft, FaBook } from 'react-icons/fa'
import { FiList, FiHome } from 'react-icons/fi'

export const links = [
    {
        label: "Home",
        icon: FiHome,
        to: '',
    },
    {
        label: "Liste Cours",
        icon: FiList,
        to: 'list-cours',
    }
]

export const cards = [
    {
        title: 'Liste Cours',
        description: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of',
        Icon: FaBook
    },
    {
        title: 'Liste Cours',
        description: 'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and',
        Icon: FaAccusoft
    },
    {
        title: 'Liste Cours',
        description: '"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi,',
        Icon: Fa500Px
    },
]