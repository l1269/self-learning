import React, { useState, useEffect } from "react";
import { db, storage } from "../../../firebase/config";
import { addDoc, collection, getDocs, query, serverTimestamp, where } from "firebase/firestore";
import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import { toast } from "react-toastify";
import { FiDelete, FiUpload } from "react-icons/fi";
import { BsUpload } from "react-icons/bs";
import "./AjoutCoursModal.css";
import { updateCurrentUser } from "firebase/auth";
import { useAuth } from "../../../hooks/useAuth";

const AjoutCoursModal = ({ setAddCourseModal, addCourseModal }) => {
  const [error, setError] = useState(null);
  const { setShowHeader } = useAuth();

  let data = {
    date: "",
    titre: "",
    url: "",
    description: "",
    img: "",
  };

  const [file, setFile] = useState("");
  const [domaine, setDomaine] = useState('')

  const [addData, setAddData] = useState(data);
  const [percent, setPercent] = useState(0);

  const { currentUser } = useAuth();
  const [auth, setAuth] = useState([]);

  const getAuth = async () => {
    const q = query(
      collection(db, "auth"),
      where("email", "==", currentUser.email)
    );
    const querySnapshot = await getDocs(q);
    querySnapshot.forEach((doc) => {
      setAuth(querySnapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() })));
    });
  };
  getAuth();

  useEffect(() => {
    const uploadFile = () => {
      const storageRef = ref(storage, file.name);
      const uploadTask = uploadBytesResumable(storageRef, file);

      uploadTask.on(
        "state_changed",
        (snapshot) => {
          const progress =
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          // console.log("Upload is" + progress + "%");
          setPercent(progress);
          switch (snapshot.state) {
            case "paused":
              console.log("Upload is paused");
              break;
            case "running":
              console.log("Upload is running");
              break;

            default:
              break;
          }
        },
        (error) => console.log(error),
        () => {
          getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
            setAddData((prev) => ({ ...prev, img: downloadURL }));
          });
        }
      );
    };
    file && uploadFile();
  }, [file]);

  // state image
  const handleUpload = (e) => {
    setFile(e.target.files[0]);
  };

  // Get inputs values
  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    setAddData({ ...addData, [name]: value });
  };

  const [loading, setLoading] = useState(false);

  // Notification
  const notification = () => toast("Cours Ajouté");


  const handleSubmit = async (e) => {
    e.preventDefault();

    if (addData.domaine === "" && auth[0]?.status === 'admin') {
      return setError("Veuillez selectionnner un domaine");
    }
    if (!file) {
      return setError("Veuillez ajouter une image de couverture");
    }

    try {
      // vider le message d'erreur d'abord s'il y'en a un
      setError("");
      // commencer le chargement jusqu'à ce que les données soient ajouté dans firebase et firestore
      setLoading(true);

      if (auth[0]?.status !== 'admin') {
        await addDoc(collection(db, "cours"), {
          ...addData,
          domaine: auth[0]?.domaine,
          assigne: [],
          isArchiver: false,
          archiverPar: null,
          auteur: auth[0]?.prenom + ' ' + auth[0]?.nom,
          auteurEmail: auth[0]?.email,
          timeStamp: serverTimestamp(),
        });
      } else {
        await addDoc(collection(db, "cours"), {
          ...addData,
          domaine: domaine,
          assigne: [],
          isArchiver: false,
          archiverPar: null,
          auteur: 'Admin',
          auteurEmail: auth[0]?.email,
          timeStamp: serverTimestamp(),
        });
      }

      console.log(data);

      setAddData({ ...data });
      setFile("");
      // arreter le chargement après que les infos aient été ajouter dans firebase et firestore
      setLoading(false);
      notification();

      setAddCourseModal(false);
    } catch (error) {
      return setError("Une erreur est survenue");
    }
  };

  const hideAddCoursModal = () => {
    setAddCourseModal(!addCourseModal);
    setShowHeader(true);
  };

  // addData destructuration
  const { date, titre, url, description } = addData;

  const btnStyle = {
    before: {
      borderRadius: 10,
      backgroundColor: "rgba(255, 0,0, 0.5)",
      color: "white",
      fontWeight: 700,
      padding: "8px 20px",
      width: "100%",
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      cursor: "pointer",
    },
    after: {
      borderRadius: 10,
      backgroundColor: "green",
      color: "white",
      fontWeight: 700,
      padding: "8px 20px",
      width: "100%",
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      cursor: "pointer",
    },
  };
  return (
    <div className="overLayAdd" id="modal">
      <form className=" p-2 row form-add" onSubmit={handleSubmit}>
        <FiDelete
          className="btn-cancel"
          style={{ cursor: "pointer" }}
          size={30}
          onClick={hideAddCoursModal}
        />
        <h2 className="text-center py-2" >Ajouter un cours</h2>

        {error && (
          <div className="border border-danger text-danger my-4 p-2 rounded-2 text-center">
            {error}
          </div>
        )}
        <div className="mb-4">
          <label
            htmlFor="file"
            style={{ display: "flex", justifyContent: 'center', flexWrap: "nowrap", width: "250px", margin: '0 auto' }}
          >
            <span style={percent === 100 ? btnStyle.after : btnStyle.before}>
              <FiUpload style={{ marginRight: 20 }} size={20} />
              {percent && percent < 100 ? (
                <div className="loading"></div>
              ) : percent === 100 ? (
                file?.name.substr(0, 30) + "..."
              ) : (
                "selectionner une image"
              )}
            </span>
          </label>
          <input
            type="file"
            id="file"
            onChange={handleUpload}
            style={{ display: "none" }}
          />
        </div>

        <div className="col-12 col-sm-12 col-md-12 mb-3">
          {
            auth[0] && auth[0]?.status === "admin" && (
              <select
                className="input-modal"
                name={"domaine"}
                value={domaine}
                onChange={(e) => setDomaine(e.target.value)}
                required
                placeholder="Mettre le contenu du cours..."
              >
                <option value="">Selectionnner le domaine</option>
                <option value="programmation">Programmation</option>
                <option value="design">Design</option>
                <option value="marketing">Marketing Digital</option>
                <option value="entrepreneuriat">Entrepreneuriat</option>
              </select>
            )
          }
        </div>

        <div className="col-12 col-sm-12 col-md-12 mb-3">
          <input
            type="text"
            className="input-modal"
            placeholder="Titre"
            value={titre}
            name={"titre"}
            onChange={handleChange}
            required
          />
        </div>

        <div className="col-12 col-sm-12 col-md-12 mb-3">
          <input
            type="url"
            className="input-modal"
            placeholder="Lien du cours"
            value={url}
            name={"url"}
            onChange={handleChange}
            required
          />
        </div>

        <div className="col-12 col-sm-12 col-md-12 mb-3">
          <input
            type="date"
            className="input-modal"
            placeholder="Date du cours"
            value={date}
            name={"date"}
            onChange={handleChange}
            required
          />
        </div>

        <div className="col-12 col-md-12 mb-3">
          <textarea
            cols="30"
            rows="4"
            placeholder="Description"
            value={description}
            name={"description"}
            onChange={handleChange}
          ></textarea>
        </div>

        <button
          className="fs-5 px-4 mb-3 btn-ajouter"
          disabled={percent === 100 ? false : true}
          style={{ maxWidth: 300 }}
        >
          {loading ? <div className="loading"></div> : "Enregistrer"}
        </button>
      </form>
    </div>
  );
};

export default AjoutCoursModal;
