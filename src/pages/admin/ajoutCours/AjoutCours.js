import React, { useState } from 'react'
import { addDoc, collection } from 'firebase/firestore';
// import { onAuthStateChanged } from 'firebase/auth';
import { db, } from '../../../firebase/config';
import './AjoutCours.css'
import { toast } from 'react-toastify';


const AjoutCours = () => {


  // State pour message d'erreur ou de réussite
  const [error, setError] = useState(null);

  // State pour les différents champs du formulaire
  const [duree, setDuree] = useState("");
  const [titre, setTitre] = useState("");
  const [nomAjouteur, setNomAjouteur] = useState("")
  const [description, setDescription] = useState("");
  const [domaine, setDomaine] = useState("");

  // State pour voir s'il faut charger ou pas en fonction du click de l'utilisateur sur le bouton ajouter
  const [loading, setLoading] = useState(false);

  // Notification
  const notification = () => toast("Cours ajoute");

  const handleSubmit = async e => {
    e.preventDefault();


    if (domaine === "") {
      return setError("Veuillez selectionnner un domaine")
    }

    try {
      // vider le message d'erreur d'abord s'il y'en a un
      setError("");
      // commencer le chargement jusqu'à ce que les données soient ajouté dans firebase et firestore
      setLoading(true);
      const data = {
        domaine,
        titre,
        duree,
        nomAjouteur,
        description,
        assigne: []
      }

      await addDoc(collection(db, 'cours'), data);
      // arreter le chargement après que les infos aient été ajouter dans firebase et firestore
      setLoading(false)
      notification()
      // vider tous les champs du formulaire
      setDomaine("")
      setTitre("")
      setDuree("")
      setNomAjouteur("")
      setDescription("")

    } catch (error) {
      return setError("Une erreur est survenue");
    }
  }

  return (
    <div style={{ maxWidth: '600px' }} className="mx-auto p-0">
      <form style={{ width: '100%' }} onSubmit={handleSubmit} className='mx-sm-auto p-sm-4 p-2'>
        <h2 className='text-center py-4'>Ajouter Cours</h2>
        {
          error && <div className='border border-danger text-danger my-4 p-2 rounded-2 text-center'>{error}</div>
        }
        <div className="mb-4">
          <select
            className='select'
            value={domaine}
            onChange={e => setDomaine(e.target.value)}
            required
          >
            <option value="">Selectionnez le domaine</option>
            <option value="programmation">Programmation</option>
            <option value="design">Design</option>
            <option value="marketing">Marketing Digital</option>
            <option value="entrepreneuriat">entrepreneuriat</option>
          </select>
        </div>

        <div className="mb-4">
          <input
            type="text"
            className="input"
            placeholder='Titre'
            value={titre}
            onChange={e => setTitre(e.target.value)}
            required
          />
        </div>

        <div className="mb-4">
          <input
            type="text"
            className='input'
            placeholder='Durée'
            value={duree}
            onChange={e => setDuree(e.target.value)}
            required
          />
        </div>

        <div className="mb-4">
          <input
            type="text"
            className='input'
            placeholder='Votre nom'
            value={nomAjouteur}
            onChange={e => setNomAjouteur(e.target.value)}
            required
          />
        </div>

        <div className="mb-4">
          <textarea
            cols="30"
            rows="10"
            value={description}
            onChange={e => setDescription(e.target.value)}
          ></textarea>
        </div>

        <button className='btn-ajouter fs-5 px-4 mb-4' type='submit'>
          {
            loading ? <div className="loading"></div> : "Ajouter"
          }
        </button>

      </form>
    </div>
  )
}

export default AjoutCours