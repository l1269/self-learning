import React from "react";
import "./HeaderApprenant.css";
import header2 from "../../assets/images/header2.svg";

function HeaderApprenant() {
  // Mon style css
  const styles = {
    container: {
      width: "100%",
      borderRadius: 7,
      padding: 10,
      display: "flex",
      alignItems: "center",
      justifyContent: "space-around",
      marginBottom: 80,
    },
    text: {
      fontFamily: "sans-serif",
      color: "rgba(231, 231, 221, 1)",
    },
  };

  return (
    <div className="header-container">
      <div className="left">
        <h3>Apprenants</h3>
        <ul>
          <li className="mt-3">
            « La formation est l’essence de tout succès. »
          </li>

          <li className="mt-3">
            « La formation révèle l’aptitude et le terrain révèle la compétence.
            »
          </li>
          <li className="mt-3">
            « Apprendre c’est avoir un projet, c’est se projeter différent dans
            l’avenir. »
          </li>
          <li className="mt-3">
            « Rien ne prédispose plus au conformisme que le manque de formation.
            »
          </li>
          <li className="mt-3">
            « Nous avons une raison de vivre : apprendre, découvrir, être libre.
            »
          </li>
        </ul>
      </div>
      <div className="right">
        <img width={"100%"} height={"100%"} src={`${header2}`} alt="header" />
      </div>
    </div>
  );
}

export default HeaderApprenant;
