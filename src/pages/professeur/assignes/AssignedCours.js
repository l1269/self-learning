import { collection, onSnapshot } from 'firebase/firestore';
import React, { useEffect, useState } from 'react'
import { db } from '../../../firebase/config';
import AssignedLayout from '../layouts/AssignedLayout'
import AssignedCoursCard from './AssignedCoursCard'

function AssignedCours({ onClose, apprenant }) {

    const [cours, setCours] = useState([]);

    const coursCollectionRef = collection(db, "cours");

    useEffect(() => {
        const getCours = async () => {
            // const q = query(coursCollectionRef, orderBy("timeStamp", "desc"));
            onSnapshot(coursCollectionRef, (querySnapshot) => {
                const usersInfo = [];
                querySnapshot.forEach((doc) => {
                    usersInfo.push({ ...doc.data(), id: doc.id });
                    setCours([...usersInfo]);
                });
                // setLoading(false);
            });
        };
        getCours();
    }, []);

    const coursAssigned = cours.filter(el => el.assigne?.includes(apprenant.id))

    return (
        <AssignedLayout title={'Les cours assignes'} onClose={onClose}>

            {coursAssigned?.length === 0 ? (
                <p>Pas de cours assigne a <strong>{`${apprenant.prenom} ${apprenant.nom}`}</strong></p>
            ) : (
                coursAssigned.map(cour => (
                    <AssignedCoursCard key={cour.id} {...cour} />
                ))
            )}

        </AssignedLayout>
    )
}

export default AssignedCours