import React from 'react'
import {FaRegSadTear} from "react-icons/fa"
import { useNavigate } from 'react-router-dom'
import errorPageImg from "../../assets/images/no-results.png"
import "./errorPage.css"

const ErrorPage = () => {

     const navigate = useNavigate();
  return (
    <div className='wrapper'>
          {/* <FaRegSadTear size={200} /> */}
          <img src={errorPageImg} alt="page non trouvé" style={{ width: '200px' }} />
          <p>404 ERROR</p>
          <p>OOPPSS !!! the page you are looking for does not exist</p>
          <button className='button' onClick={() =>navigate(-1)}>Go back</button>
    </div>
  )
}

export default ErrorPage