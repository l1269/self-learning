import React, { useEffect, useState } from "react";
import ListCours from "../listCours/ListCours";
import { Routes, Route, useNavigate } from "react-router-dom";
import { db } from "../../../firebase/config";
import { collection, onSnapshot, query, where, getDocs } from 'firebase/firestore'
import CoursArchives from "../coursArchives/CoursArchives";
import SidebarMob from "../../admin/adminSidebar/SidebarMob";
import "../../admin/dashbord/AdminDashboard.css";
import Header from "../../../components/header/Header.js";
import HeaderMob from "../../../components/header/HeaderMob.js";
import SideBar from "../../../components/SideBar/SideBar.js";
import { links } from "../../../utils/ProfesseurData";
import { Navigate } from "react-router-dom";
import { useAuth } from "../../../hooks/useAuth.js";
import { CountProvider } from "../../../context/CountProvider.js";
import ListeApprenants from "../listeApprenants/ListeApprenants";
import ErrorPage from "../../../components/errorPage/ErrorPage";
import './DashboardProfesseur.css'
import { resize } from "../../../utils/resize";

const DashboardProfesseur = () => {
  const { currentUser } = useAuth();

  const [profInfo, setProfInfo] = useState(null)

  const navigate = useNavigate();


  useEffect(() => {
    /*****************************************/
    //  recuperer dans firestore l'email et le statut de l'utilisateur actuel et le mettre dans profInfo
    const q = query(collection(db, "auth"), where("email", "==", `${currentUser.email}`));

    onSnapshot(q, querySnapshot => {

      querySnapshot.forEach((doc) => {
        const {email, status} = doc.data();
        setProfInfo({email, status});
      });

    })
    /*****************************************/

    /*
      vérifier si l'email du current user est egale à l'email de profInfo et
      verifier si le statut de profInfo est différent de professeur.
      si les deux sont vrais on retourne dans la page précédente
    */
    if(currentUser.email === profInfo?.email && profInfo?.status !== "professeur") {
      navigate(-1);
    }
  }, [currentUser.email, navigate, profInfo])


  const [auth, setAuth] = useState([])


  useEffect(() => {
    const getAuth = async () => {
      const q = query(
        collection(db, "professeur"),
        where("email", "==", currentUser.email)
      );
      const querySnapshot = await getDocs(q);
      querySnapshot.forEach((doc) => {
        setAuth(
          querySnapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }))
        );
      });
    };
    getAuth();
  }, []);

  useEffect(() => {
    let width = document.getElementById('sidebar').offsetWidth;
    document.querySelector('.right-side').style.paddingLeft = `${width}px`;
  }, [])

  resize()


  if (!currentUser) {
    return <Navigate to={"/"}></Navigate>;
  }

  

  return (
    <div className="dashboard background">
      <div
        style={{ maxWidth: "230px" }}
        className="left-side backSide p-0"
      >
        <CountProvider>
          <SideBar links={links} />
          <SidebarMob links={links} />
        </CountProvider>
      </div>
      <div className="right-side right-side-apprenant">
        <Header />
        <HeaderMob title="Espace de travail Professeur" />
        <div className="containt">
          <div className="main-containt table-responsive">
            <Routes>
              <Route index element={<ListCours />} />
              <Route path="cours-archives" element={<CoursArchives />} />
              <Route path="*" element={<ErrorPage/>} />
            </Routes>
          </div>
          <div className="aside-containt">
            <div className="p-0">
              <ListeApprenants professeur={auth[0]} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DashboardProfesseur;
