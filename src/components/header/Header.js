import React, { useEffect, useState } from "react";
import "./Header.css";
import { AiOutlineLogout } from "react-icons/ai";
import { BsPersonCircle } from "react-icons/bs";
import logo from "../../assets/design/logo2.png";
import { useAuth } from "../../hooks/useAuth";
import { useNavigate } from "react-router-dom";
import { collection, getDocs, query, where } from "firebase/firestore";
import { db } from "../../firebase/config";


const Header = () => {
  const { logout, currentUser } = useAuth();
  const navigate = useNavigate();
  const [auth, setAuth] = useState([]);

  useEffect(() => {
    const getAuth = async () => {
      const q = query(
        collection(db, "auth"),
        where("email", "==", currentUser.email)
      );
      const querySnapshot = await getDocs(q);
      querySnapshot.forEach((doc) => {
        setAuth(
          querySnapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }))
        );
      });
    };
    getAuth();
  }, []);
  const signout = async () => {
    try {
      await logout();
      navigate("/");
    } catch {
      alert(`Une erreur s'est produite !`);
    }
  };
  return (
    <header id="header" className="text-dark px-3 py-2 w-100 shadow d-block d-sm-none sticky-top">
      <div className="d-flex justify-content-between align-items-center">
        <img
          className="me-lg-1 logo-img d-sm-none d-inline"
          style={{ maxWidth: "40px", marginTop: "-25px" }}
          src={logo}
          alt="Logo"
        />

        <div className="user-infos">
          <div className="dropdown">
            <button
              className="dropdown-toggle drop"
              type="button"
              id="dropdown"
              data-bs-toggle="dropdown"
              aria-expanded="false"
            >
              <BsPersonCircle style={{ fontSize: "2rem" }} />
              {auth.length !== 0
                ? `${auth[0]?.prenom} ${auth[0]?.nom}`
                : "loading..."}
            </button>
            <ul className="dropdown-menu py-0 me-0" aria-labelledby="dropdown">
              <li className="text-center bg-danger opacity-95 rounded py-2 px-0">
                <button
                  style={{ border: "none", backgroundColor: "transparent" }}
                  onClick={signout}
                  className="p-0 text-light"
                >
                  <AiOutlineLogout style={{ fontSize: "1.2rem" }} />
                  Deconnexion
                </button>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
