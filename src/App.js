import {Routes, Route} from 'react-router-dom'
import Login from './components/login/Login'
import Redirect from './components/redirect/Redirect';
import Signup from './components/signup/Signup'
import {AuthProvider} from './context/AuthProvider'
import AdminDashboard from './pages/admin/dashbord/AdminDashboard'
import { ToastContainer } from 'react-toastify';
import ApprenantDashboard from './pages/apprenant/dashbord/ApprenantDashboard';
import DashboardProfesseur from './pages/professeur/dashboard/DashboardProfesseur';
import ErrorPage from './components/errorPage/ErrorPage';


function App() {
  return (
    <div>
      <AuthProvider>
        <Routes>
          <Route path='/' element={<Login />} />
          <Route path='/adminbi' element={<Signup />} />
          <Route path='/redirect' element={<Redirect />} />
          <Route path='/admin/*' element={<AdminDashboard />} />
          <Route path='/apprenant/*' element={<ApprenantDashboard />} />
          <Route path='/professeur/*' element={<DashboardProfesseur />} />
          <Route path='*' element={<ErrorPage/>}/>
        </Routes>
        <ToastContainer />
      </AuthProvider>
    </div>
  );
}
export default App;