import React from "react";
import Schimer from "./Shimer";
import SkeletonElement from "./SkeletonElement";

const SkeletonCours = () => {
  return (
    <div className="skeleton-wrapper">
      <div className="skeleton-cours">
        <SkeletonElement type="thumbnail" />
        <SkeletonElement type="title" />
        <SkeletonElement type="button" />
      </div>
      <Schimer  />
    </div>
  );
};

export default SkeletonCours;
