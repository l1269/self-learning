
export const resize = () => {
  window.addEventListener("resize", () => {
    let width = document.getElementById("sidebar").offsetWidth;
    document.querySelector(".right-side").style.paddingLeft = `${width}px`;
  });
};
