import React, { useState, useEffect } from 'react';
import emailjs from '@emailjs/browser';
import { useAuth } from '../../../hooks/useAuth';
import { toast } from 'react-toastify';
import "./AjoutUtilisateurModal.css";
import { FiDelete } from 'react-icons/fi';
import { db } from '../../../firebase/config';
import { addDoc, collection, serverTimestamp, orderBy, query, getDocs, where, onSnapshot } from 'firebase/firestore';

const AjoutUtilisateurModal = ({ setShowUserModal, statut }) => {


  const { signup, currentUser, editCurrentUser } = useAuth();

  const [lesProfs, setLesProfs] = useState([]);

  const AdminCurrentUser = currentUser;

  useEffect(() => {
    const getProfs = async () => {
      const q = query(
        collection(db, "professeur"),
        orderBy("timeStamp", "desc")
      );
      onSnapshot(q, (querySnapshot) => {
        const usersInfo = [];
        querySnapshot.forEach((doc) => {
          usersInfo.push({ ...doc.data(), id: doc.id });
          setLesProfs([...usersInfo]);
        });
        setLoading(false);
      });
    };
    getProfs();
  }, []);

  // Notification
  const notificationStudent = () => toast("Apprenant ajouté");
  const notificationProf = () => toast("Professeur ajouté");

  // State pour message d'erreur ou de réussite
  const [error, setError] = useState(null);

  const user = {
    nom: "",
    prenom: "",
    telephone: "",
    domaine: "",
    coach: "",
    email: "",
    password: "",
    avatar: null,
    assigne: [],
    confirmPassword: "",
  };

  const [data, setData] = useState(user);

  const [loading, setLoading] = useState(false);

  // regex pour voir si le champ nom et le champ prenom contiennent que des caractères de l'alphabet
  const regexNomPrenom = /^[a-zA-Zéèêïô-]+$/;

  // regex pour verifier si l'email est en format valide
  const regexEmail =
    /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

  const {
    nom,
    prenom,
    telephone,
    email,
    password,
    confirmPassword,
    coach,
    domaine,
    assigne,
  } = data;

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!regexNomPrenom.test(nom) || !regexNomPrenom.test(prenom)) {
      return setError("Remplissez correctement le nom ou le prenom !!!!");
    }

    if (!regexEmail.test(email)) {
      return setError("Adresse email invalide");
    }

    if (domaine === "") {
      return setError("Veuillez ajouter une filière");
    }
    ///////////////////////////////
    let emails = [];

    const q = query(collection(db, "auth"), where("email", "==", `${email}`));

    const querySnapshot = await getDocs(q);
    querySnapshot.forEach((doc) => {
      emails.push(doc.data());
    });

    if (emails.length !== 0) {
      return setError("Adresse email déja utilisé");
    }
    ///////////////////////////////

    if (password.length < 6) {
      return setError("Mot de passe court (au moins 6 caracteres)");
    }

    if (password !== confirmPassword) {
      return setError("Les mots de passe ne correspondent pas");
    }

    try {
      setError("");
      setLoading(true);

      await signup(email, password);

      await editCurrentUser(AdminCurrentUser);

      const data =
        statut === "professeur"
          ? {
            nom,
            prenom,
            email,
            password,
            telephone,
            domaine,
            avatar: null,
            assigne: [],
            statut,
            isArchiver: false,
            timeStamp: serverTimestamp(),
          }
          : {
            nom,
            prenom,
            email,
            password,
            telephone,
            domaine,
            coach,
            assigne,
            statut,
            isArchiver: false,
            timeStamp: serverTimestamp(),
          };

      await addDoc(collection(db, "auth"), {
        prenom,
        nom,
        email,
        status: statut,
        domaine,
      });

      await addDoc(collection(db, `${statut}`), data);

      setLoading(false);

      if (statut === "apprenant") notificationStudent();
      if (statut === "professeur") notificationProf();

      setData(user);

      setShowUserModal(false);
    } catch (error) {
      setLoading(false);
      if (error.code === "auth/invalid-email")
        return setError(`L'adresse email est invalide !`);
      if (error.code === "auth/user-not-found")
        return setError("Utilisateur introuvable !");
      if (error.code === "auth/wrong-password")
        return setError("Email ou Mot de passe incorrecte");
      if (error.code === "auth/email-already-in-use")
        return setError("Adresse email déja utilisé");
    }
  };

  return (
    <div>
      <div className="overlayModal">
        <form
          className="p-sm-4 mx-auto p-3 row form-add-user"
          onSubmit={handleSubmit}
        >
          <FiDelete
            className="btn-cancel"
            style={{ cursor: "pointer" }}
            size={25}
            onClick={() => setShowUserModal(false)}
          />
          <h2 className="text-center py-4">
            {statut === "professeur"
              ? "Ajouter Professeur"
              : "Ajouter Apprenant"}
          </h2>
          {error && (
            <div className="border border-danger text-danger my-4 p-2 rounded-2 text-center">
              {error}
            </div>
          )}
          <div className="col-12 col-sm-6 col-md-6 mb-4">
            <input
              type="text"
              className="input-modal"
              placeholder="Nom"
              value={nom}
              name="nom"
              onChange={handleChange}
              required
            />
          </div>

          <div className="col-12 col-sm-6 col-md-6 mb-4">
            <input
              type="text"
              className="input-modal"
              placeholder="Prénom"
              value={prenom}
              name="prenom"
              onChange={handleChange}
              required
            />
          </div>

          <div className="col-12 col-sm-6 col-md-6 mb-4">
            <input
              type="tel"
              className="input-modal"
              placeholder="777777777"
              pattern="^(77|78|70|76)[0-9]{3}[0-9]{2}[0-9]{2}"
              value={telephone}
              name="telephone"
              onChange={handleChange}
              required
            />
          </div>

          <div className="col-12 col-sm-6 col-md-6 mb-4">
            <select
              // id="selected"
              value={domaine}
              onChange={handleChange}
              name="domaine"
              className="input-modal"
              aria-label="Default select example"
            >
              <option value="">filières</option>
              <option value="programmation">Programmation</option>
              <option value="design">Design</option>
              <option value="marketing">Marketing</option>
              <option value="entrepreneuriat">Entrepreneuriat </option>
            </select>
          </div>

          {statut === "apprenant" && (
            <div className="col-12 col-sm-6 col-md-6 mb-4">
              <select
                className="input-modal"
                value={coach}
                name="coach"
                onChange={handleChange}
              >
                {


                    lesProfs?.filter((prof) => prof.domaine === domaine)
                      .length !== 0 ? (
                      <>
                        <option value="">
                          Professeur
                        </option>
                        {lesProfs
                          ?.filter((prof) => prof.domaine === domaine)
                          .map((prof, index) => (
                            <option
                              key={index}
                              value={prof.id}
                            >{`${prof.prenom} ${prof.nom}`}</option>
                          ))}
                      </>
                    ) : (
                      <option value="">
                        il n'y a pas de professeur dans ce domaine{" "}
                      </option>
                    )


                  // ) : (
                  //   <option value="">Veuillez selectionner son professeur</option>
                  // )
                }
              </select>
            </div>
          )}


          <div className=" col-12 col-sm-6 col-md-6 mb-4">
            <input
              type="email"
              className="input-modal"
              placeholder="Email"
              name="email"
              value={email}
              onChange={handleChange}
              required
            />
          </div>

          <div className="col-12 col-sm-6 col-md-6 mb-4">
            <input
              type="password"
              className="input-modal"
              placeholder="Mot de passe"
              name="password"
              value={password}
              onChange={handleChange}
              required
            />
          </div>

          <div className="col-12 col-sm-6 col-md-6 mb-4">
            <input
              type="password"
              className="input-modal"
              placeholder="Confirmer Mot de passe"
              name="confirmPassword"
              value={confirmPassword}
              onChange={handleChange}
              required
            />
          </div>

          <button className="btn-ajouter fs-5 px-4 mb-4">
            {loading ? <div className="loading"></div> : "Ajouter"}
          </button>
        </form >
      </div >
    </div >
  );
};

export default AjoutUtilisateurModal;
