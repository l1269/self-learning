import React, { useEffect, useState } from "react";
import { AiOutlineLogout } from "react-icons/ai";
import { useNavigate } from "react-router-dom";
import { collection, getDocs, query, where } from "firebase/firestore";
import { useAuth } from "../../../hooks/useAuth";
import { db } from "../../../firebase/config";
import logo from '../../../assets/design/logo2.png'

function Header() {

    const { logout, currentUser } = useAuth();
    const navigate = useNavigate();
    const [auth, setAuth] = useState([]);

    useEffect(() => {
        const getAuth = async () => {
            const q = query(
                collection(db, "auth"),
                where("email", "==", currentUser.email)
            );
            const querySnapshot = await getDocs(q);
            querySnapshot.forEach((doc) => {
                setAuth(
                    querySnapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }))
                );
            });
        };
        getAuth();
    }, []);
    const signout = async () => {
        try {
            await logout();
            navigate("/");
        } catch {
            alert(`Une erreur s'est produite !`);
        }
    };

    return (
        <div
            style={{
                width: '100%',
                height: 60,
                display: 'flex',
                alignItems: 'center',
                backgroundColor: 'white',
                position: 'fixed',
                top: 0,
                zIndex: 99
            }}
        >
            <div
                style={{
                    width: '85%',
                    margin: '0 auto',
                    display: 'flex',
                    justifyContent: 'space-between'
                }}
            >
                {/* logo */}
                <div style={{ display: 'flex', alignItems: 'center'}}>
                    <img className='logo-img' style={{ maxWidth: '40px' }} src={logo} alt="Logo" />
                    <h3 className='' style={{ fontWeight: 400}}>Learning</h3>
                </div>


                <div className="">
                    <div className="dropdown">
                        <button
                            className="dropdown-toggle drop"
                            type="button"
                            id="dropdown"
                            data-bs-toggle="dropdown"
                            aria-expanded="false"
                        >
                            {auth.length !== 0 ? `${auth[0]?.prenom} ${auth[0]?.nom}` : 'loading...'}
                        </button>
                        <ul className="dropdown-menu py-0 me-0" aria-labelledby="dropdown">
                            <li className="text-center bg-danger opacity-95 rounded  px-0">
                                <button
                                    style={{ border: "none", backgroundColor: "transparent" }}
                                    onClick={signout}
                                    className="p-0 text-light"
                                >
                                    <AiOutlineLogout style={{ fontSize: "1.2rem" }} />
                                    Deconnexion
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Header