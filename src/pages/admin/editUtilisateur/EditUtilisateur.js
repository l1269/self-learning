import React, { useState, useEffect } from "react";
import { db } from "../../../firebase/config";
import {
  doc,
  updateDoc,
  getDocs,
  collection,
  query,
  where,
  deleteDoc,
  addDoc,
  serverTimestamp,
  onSnapshot,
  orderBy,
} from "firebase/firestore";
import "./EditUtilisateur.css";
import { FiDelete } from "react-icons/fi";
import { toast } from "react-toastify";

function EditUtilisateur({ showModal, setShowModal, savedUser, user }) {
  const [lesProfs, setLesProfs] = useState([]);
  const [studentWithMissingTeacher, setStudentWithMissingTeacher] = useState(
    []
  );
  

  useEffect(() => {
    const getProfs = async () => {
      const q = query(
        collection(db, "professeur"),
        orderBy("timeStamp", "desc")
      );
      onSnapshot(q, (querySnapshot) => {
        const usersInfo = [];
        querySnapshot.forEach((doc) => {
          usersInfo.push({ ...doc.data(), id: doc.id });
          setLesProfs([...usersInfo]);
        });
        setLoading(false);
      });
    };
    getProfs();
  }, []);

  const oldNom = savedUser.nom;
  const oldPrenom = savedUser.prenom;
  const oldTelephone = savedUser.telephone;
  const oldStatut = savedUser.statut;
  const idDoc = savedUser.id;
  const email = savedUser.email;
  const password = savedUser.password;
  const oldDomaine = savedUser.domaine;
  const oldCoach = savedUser.coach;
  const { assigne, avatar, isArchiver } = savedUser;
  
  // State pour voir s'il faut charger ou pas en fonction du click de l'utilisateur sur le bouton ajouter
  const [loading, setLoading] = useState(false);
  
  // State pour message d'erreur ou de réussite
  const [error, setError] = useState(null);
  
  // State pour les différents champs du formulaire
  const [nom, setNom] = useState(oldNom);
  const [prenom, setPrenom] = useState(oldPrenom);
  const [telephone, setTelephone] = useState(oldTelephone);
  const [statut, setStatut] = useState(oldStatut);
  const [domaine, setDomaine] = useState(oldDomaine);
  const [coach, setCoach] = useState(oldCoach);

  useEffect(() => {
    const getApprenants = async () => {
      const q = query(
        collection(db, "apprenant"),
        where("coach", "==", `${savedUser.id}`)
      );

      onSnapshot(q, (querySnapshot) => {
        const usersInfo = [];
        querySnapshot.forEach((doc) => {
          usersInfo.push({ ...doc.data(), id: doc.id });
          setStudentWithMissingTeacher([...usersInfo]);
        });
        setLoading(false);
      });
    };

    getApprenants();
    
    
    if ((oldStatut === "professeur" && statut === "apprenant") || (oldDomaine !== domaine) ) {
      studentWithMissingTeacher.forEach(async (student) => {
        const docRef = doc(db, "apprenant", student.id);

        await updateDoc(docRef, { coach: "" });
      });
    }

    
    
  }, [statut, domaine]);

  // const [isStudent, setIsStudent] = useState(() => statut === 'apprenant' ? true : false);

  // const regexNomPrenom = /^[a-zA-Z]+$/;
  const regexNomPrenom = /^[a-zA-Zéèêïô-]+$/;

  // Notification
  const notification = () => toast(`${user} modifie`);

  // const handleStatut = e => {
  //   setStatut(e.target.value)
  //   if(e.target.value === 'apprenant') {
  //     setIsStudent(true);
  //   }
  //   else {
  //     setIsStudent(false);
  //   }
  // }

  const handleSubmit = async (e) => {
    e.preventDefault();

    
    if (nom === "" || prenom === "" || telephone === "" || statut === "") {
      setLoading(false);
      return setError("veuillez renseigner les champs vides");
    }

    if (!regexNomPrenom.test(nom) || !regexNomPrenom.test(prenom)) {
      setLoading(false);
      return setError("Remplissez correctement le nom ou le prenom !!!!");
    }

    if(domaine === "") {
      setLoading(false)
      return setError("veuillez choisir une filière");
    }

    try {
      // vider le message d'erreur d'abord s'il y'en a un
      setError("");
      setLoading(true);

      if (oldStatut === statut) {
        const docRef = doc(db, `${oldStatut}`, `${idDoc}`);

        const data =
          statut === "apprenant"
            ? {
                nom,
                prenom,
                telephone,
                domaine,
                coach,
                timeStamp: serverTimestamp(),
              }
            : {
                nom,
                prenom,
                telephone,
                domaine,
                timeStamp: serverTimestamp(),
              };

        const dataForAuth = {
          nom,
          prenom,
          status: statut,
          domaine,
        };

        await updateDoc(docRef, data);

        const q = query(collection(db, "auth"), where("email", "==", email));

        const querySnapshot = await getDocs(q);
        querySnapshot.forEach(async (document) => {
          const docRef = doc(db, "auth", document.id);

          await updateDoc(docRef, dataForAuth);
        });
      } else {
        const dataForAuth = {
          nom,
          prenom,
          status: statut,
          domaine,
        };

        const dataAlmostComplete =
          statut === "apprenant"
            ? {
                nom,
                prenom,
                email,
                password,
                telephone,
                domaine,
                coach,
                assigne,
                statut,
                isArchiver,
                timeStamp: serverTimestamp(),
              }
            : {
                nom,
                prenom,
                email,
                password,
                telephone,
                domaine,
                avatar,
                assigne: [],
                statut,
                isArchiver: false,
                timeStamp: serverTimestamp(),
              };

        // supprimer dans l'ancienne collection
        await deleteDoc(doc(db, `${oldStatut}`, `${idDoc}`));
        

        await addDoc(collection(db, `${statut}`), dataAlmostComplete);
        



        // mettre à jour la propriété coach des étudiants si le statut est changé en apprenant
        // if ((oldStatut === "professeur" && statut === "apprenant") || (oldDomaine !== domaine) ) {
        //   studentWithMissingTeacher.forEach(async (student) => {
        //     const docRef = doc(db, "apprenant", student.id);

        //     await updateDoc(docRef, { coach: "" });
        //   });
        // }

        // mettre à jour dans auth
        const q = query(collection(db, "auth"), where("email", "==", email));

        const querySnapshot = await getDocs(q);
        querySnapshot.forEach(async (document) => {
          const docRef = doc(db, "auth", document.id);

          await updateDoc(docRef, dataForAuth);
        });
      }
      

      // arreter le chargement après que les infos aient été ajouter dans firebase et firestore
      setLoading(false);
      
      setShowModal(!showModal);
      notification();
    } catch (error) {
      setLoading(false);
    }
  };

  return (
    <div style={{ zIndex: "999 !important" }} className="Modal">
      <form
        className="mx-sm-auto p-sm-4 p-2 form-edit"
        style={{ maxWidth: "600px" }}
        onSubmit={handleSubmit}
      >
        <FiDelete
          className="btn-delete"
          size={30}
          onClick={() => setShowModal(!showModal)}
        />

        <h2 className="text-center py-4">Modifier utilisateur</h2>
        {error && (
          <div className="border border-danger text-danger my-4 p-2 rounded-2 text-center">
            {error}
          </div>
        )}
        <div className="mb-4">
          <input
            type="text"
            className="input-modal"
            placeholder="Nom"
            value={nom}
            onChange={(e) => setNom(e.target.value)}
            required
          />
        </div>

        <div className="mb-4">
          <input
            type="text"
            className="input-modal"
            placeholder="Prénom"
            value={prenom}
            onChange={(e) => setPrenom(e.target.value)}
            required
          />
        </div>

        <div className="mb-4">
          <input
            placeholder="777777777"
            type="tel"
            className="input-modal"
            pattern="^(77|78|70|76)[0-9]{3}[0-9]{2}[0-9]{2}"
            value={telephone}
            onChange={(e) => setTelephone(e.target.value)}
            required
          />
        </div>

        {/* <div className="mb-4">
          <select
            className="input-modal"
            value={statut}
            onChange={(e) => setStatut(e.target.value)}
          >
            <option value="">Selectionnez le statut</option>
            <option value="apprenant">Apprenant</option>
            <option value="professeur">Professeur</option>
          </select>
        </div> */}
        <div className="mb-4">
          <select
            value={domaine}
            onChange={(e) => {
              setCoach("");
              setDomaine(e.target.value);
            }}
            className="input-modal"
            // aria-label="Default select example"
          >
            <option value="">filières</option>
            <option value="programmation">Programmation</option>
            <option value="design">Design</option>
            <option value="marketing">Marketing</option>
            <option value="entrepreneuriat">Entrepreneuriat </option>
          </select>
        </div>

        {statut === "apprenant" && (
          <div className="col-12 col-sm-6 col-md-6 mb-4">
            <select
              className="input-modal"
              // required = {oldDomaine !== domaine && true}
              value={coach}
              name="coach"
              onChange={(e) => setCoach(e.target.value)}
            >
              {lesProfs?.filter((prof) => prof.domaine === domaine).length !==
              0 ? (
                <>
                  <option value="">Professeur</option>
                  {lesProfs
                    ?.filter(
                      (prof) =>
                        prof.domaine === domaine && prof.id !== savedUser.id
                    )
                    .map((prof, index) => (
                      <option
                        key={index}
                        value={prof.id}
                      >{`${prof.prenom} ${prof.nom}`}</option>
                    ))}
                </>
              ) : (
                <option value="">
                  il n'y a pas de professeur dans ce domaine{" "}
                </option>
              )}
            </select>
          </div>
        )}

        <button className="btn-ajouter fs-5 px-4 mb-4" type="submit">
          {loading ? <div className="loading"></div> : "Enregistrer"}
        </button>
      </form>
    </div>
  );
}
export default EditUtilisateur;
