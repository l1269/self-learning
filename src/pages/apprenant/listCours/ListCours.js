import React, { useEffect, useState } from "react";
import ModalCours from "../../../components/modalCours/ModalCours";
import AfficheMesage from "../../../components/afficheMessage/AfficheMesage";
import Card from "../../../components/cards/Card";
import { db } from "../../../firebase/config";
import {
  collection,
  getDocs,
  onSnapshot,
  orderBy,
  query,
  where,
} from "firebase/firestore";
import { useAuth } from "../../../hooks/useAuth";
import SkeletonCours from "../../../components/skeleton/SkeletonCours";


const ListCours = ({domaine}) => {

  const { currentUser } = useAuth();
  const [auth, setAuth] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const [loading, setLoading] = useState(true);
  const [cours, setCours] = useState([]);
  const [savedCour, setSavedCour] = useState(null);
  const [listCours, setListCours] = useState([]);
  const coursCollectionRef = collection(db, "cours");

  //fonction cours

  useEffect(() => {
    const getCours = async () => {
      // const q = query(coursCollectionRef, orderBy("timeStamp", "desc"));
      onSnapshot(coursCollectionRef, (querySnapshot) => {
        const usersInfo = [];
        querySnapshot.forEach((doc) => {
          usersInfo.push({ ...doc.data(), id: doc.id });
          setCours([...usersInfo]);
          
        });
        setLoading(false);
      });
    };
   
    const getAuth = async () => {
      const q = query(
        collection(db, "apprenant"),
        where("email", "==", currentUser.email)
      );
      const querySnapshot = await getDocs(q);
      querySnapshot.forEach((doc) => {
        setAuth(
          querySnapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }))
        );
      });
    };
    getAuth();

    getCours();
  }, []);

  const handleCours = (cour) => {
    setSavedCour(cour);
    setOpenModal(true);
  };
  return (
    <>
      <h1 className="title">Liste des cours</h1>
       <h2>{domaine}</h2>
      <div
        className="filter add-course"
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginBottom: 30,
          flexWrap: "wrap",
        }}
      >
      </div>
      <div className="grid-container ">
        {!loading ? (
          listCours.length !== 0 ?
          listCours?.filter(cour => cour.domaine === auth[0]?.domaine).map((cour) => (
            <Card
              key={cour.id}
              img={cour.img}
              titre={cour.titre}
              domaine={cour.domaine}
              duree={cour.duree}
            >
              <button className="card-btn" onClick={() => handleCours(cour)}>
                Details
              </button>
            </Card>
          )) : <AfficheMesage
                message="Ajouter cours"
                error="Pas de cours pour le moment"
              ></AfficheMesage>
        ) : (
          [1,2,3,4,5].map(n => <SkeletonCours key={n} />)
        )}
        {openModal && (
          <ModalCours
            setShowModal={setShowModal}
            cours={cours}
            onClose={setOpenModal}
            savedCour={savedCour}
          />
        )}
      </div>
    </>
  );
};
export default ListCours;