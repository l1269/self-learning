import React, { useEffect, useState } from 'react'
import './NombreProf.css'
import { db } from '../../../firebase/config'
import { collection, onSnapshot } from 'firebase/firestore'
import { TbUser } from 'react-icons/tb'

export default function NombreProf() {

    const [nombre, setNombre] = useState()

    useEffect(() => {
        const getProfs = () => {
            onSnapshot(collection(db, 'professeur'), (querySnapshot) => {
                const numberProf = querySnapshot.size
                setNombre(numberProf)
            })
        }
        getProfs()
    }, [])

    const styles = {
        container: {
            width: '100%',
            height: 'auto',
            borderRadius: 7,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
            backgroundColor: '#00171F',
            borderLeft: '7px solid #fff',
            marginTop: 20,
            padding: 7,
            boxShadow: 'rgba(149, 157, 165, 0.2) 0px 8px 24px'
        },

        title: {
            fontSize: 20,
            fontWeight: 400,
            textTransform: 'uppercase',
            color: '#fff'
        },


        number: {
            textAlign: 'right',
            width: '100%',
            fontSize: 20,
            fontWeight: 500,
            color: '#fff'
        },

        icon:{
            color: '#fff'
        }
    }

    return (
        <>
            <div style={styles.container}>
                <TbUser style={styles.icon} size={60}/>
                <div style={{width: '100%', textAlign: 'right'}}>
                    <p style={styles.title}>Professeurs</p>
                    <span style={styles.number}>{nombre}</span>
                </div>
            </div>
        </>
    )
}
