import React from 'react'

const Schimer = () => {
  return (
    <div className='shimer-wrapper'>
      <div className="shimer"></div>
    </div>
  )
}

export default Schimer