import { useEffect, useState } from "react";
import { collection, getDocs, onSnapshot, orderBy, query, where } from "firebase/firestore";
import { BsBarChart, BsBrushFill, BsBuilding, BsCodeSlash, BsEmojiExpressionless, BsSearch } from "react-icons/bs";
import { Navigate, useNavigate } from "react-router-dom";
import { db } from "../../../firebase/config.js";
import { useAuth } from "../../../hooks/useAuth.js";
import { CardCours } from "../cards/CardCours.js";
import { Layout } from "../layout/Layout.js";
import ModalCours from "../../../components/modalCours/ModalCours.js";
import './Dashboard.css'
import {ImEnter} from "react-icons/im"

import Header from '../../../components/header/Header'
import HeaderMob from '../../../components/header/HeaderMob'
import AfficheMesage from "../../../components/afficheMessage/AfficheMesage.js";
import Messages from "../messages/Messages.js";

export default function ApprenantDashboard() {

  const styles = {
    searchContainer: {
      width: 500,
      height: 40,
      backgroundColor: 'white',
      borderRadius: 7,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      padding: '0 20px',
      marginRight: 20,
      boxShadow: 'rgba(0, 0, 0, 0.1) 0px 20px 25px -5px, rgba(0, 0, 0, 0.04) 0px 10px 10px -5px',
    },

    input: {
      all: 'unset',
      width: '100%',
      height: '100%',
    },

    btnSearch: {
      all: 'unset',
      height: 40,
      padding: '0 15px',
      borderRadius: '7px',
      backgroundColor: '#7a89fe',
      color: 'white',
      fontSize: 15,
      fontWeight: 400,
      boxShadow: 'rgba(0, 0, 0, 0.1) 0px 20px 25px -5px, rgba(0, 0, 0, 0.04) 0px 10px 10px -5px',
      cursor: 'pointer'
    }

  }

  const [openModal, setOpenModal] = useState(false);
  const [savedCour, setSavedCour] = useState(null);
  const [loading, setLoading] = useState(true);
  const [cours, setCours] = useState([]);
  const [listCours, setListCours] = useState([]);
  const [searchData, setSearchData] = useState('')
  const [auth, setAuth] = useState([]);
  const [apprenantInfo, setApprenantInfo] = useState([]);

  const coursCollectionRef = collection(db, "cours");

  const navigate = useNavigate();
  const { currentUser } = useAuth();



  useEffect(() => {
    const getAuth = async () => {
      const q = query(
        collection(db, "apprenant"),
        where("email", "==", currentUser.email)
      );
      const querySnapshot = await getDocs(q);
      querySnapshot.forEach((doc) => {
        setAuth(
          querySnapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }))
        );
      });
    };
    getAuth();
  }, []);

  useEffect(() => {
    const getCours = async () => {
      const q = query(coursCollectionRef, orderBy("timeStamp", "desc"));
      onSnapshot(q, (querySnapshot) => {
        const usersInfo = [];
        querySnapshot.forEach((doc) => {
          usersInfo.push({ ...doc.data(), id: doc.id });
          setCours([...usersInfo]);
          setListCours([...usersInfo]);

        });
        setLoading(false);
      });
    };
    getCours();
  }, []);


  /////////////////////////////////

  useEffect(() => {
    /*****************************************/
    //  recuperer dans firestore l'email et le statut de l'utilisateur actuel et le mettre dans profInfo
    const q = query(collection(db, "auth"), where("email", "==", `${currentUser.email}`));

    onSnapshot(q, querySnapshot => {

      querySnapshot.forEach((doc) => {
        const { email, status } = doc.data();
        setApprenantInfo({ email, status });
      });

    })
    /*****************************************/

    /*
      vérifier si l'email du current user est egale à l'email de profInfo et
      verifier si le statut de profInfo est différent de professeur.
      si les deux sont vrais on retourne dans la page précédente
    */
    if (currentUser.email === apprenantInfo?.email && apprenantInfo?.status !== "apprenant") {
      navigate(-1);
    }
  }, [])

  ///////////////////////////////

  const handleCours = (cour) => {
    setSavedCour(cour);
    setOpenModal(true);
  };

  const handleChange = (e) => {
    setSearchData(e.target.value)
  }

  if (!currentUser) {
    return <Navigate to={"/"}></Navigate>;
  }

  const singleCours = listCours?.filter(cour => cour?.assigne?.includes(auth[0]?.id))

  const styleButton = {
    all: 'unset',
    height: 30,
    padding: '0 20px',
    borderRadius: '10px',
    border: '1px solid #7a89fe',
    color: '#7a89fe',
    fontWeight: 500,
    fontSize: 15,
    cursor: 'pointer',
    marginRight: 10
  }


  return (
    <>
      <Header />
      <HeaderMob page={'apprenant'} handleSearch={handleChange} />
      <Layout id={auth[0]?.coach}>
        <div style={{ width: '85%', height: 'auto', margin: '0 auto' }}>

          {/* Search bar */}
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              marginBottom: 50
            }}
          >

            {/* <div style={styles.searchContainer}>

              <input
                style={styles.input}
                placeholder="Recherche..."
                onChange={handleChange}
              />
              <BsSearch size={20} color={'grey'} />
            </div> */}
          </div>

          <div className="containt" style={{ marginTop: -20 }}>
            <div className="">

              {/* Our cours */}
              {!loading ? (
                listCours?.length !== 0 && (
                  singleCours?.filter(cour => cour.titre.toLowerCase()?.includes(searchData.toLowerCase())).map((cour) => (
                    cour.domaine === auth[0]?.domaine && (<CardCours key={cour.id} {...cour}>
                      <button
                        onClick={() => handleCours(cour)}
                        style={styleButton}
                      >Details</button>

                      <button style={styleButton}>
                        <a href={cour.url} target="blank" id="course-link" ><ImEnter /></a>
                      </button>
                    </CardCours>
                    )))
                )) : (
                <div className="text-center mt-3">
                  <div className="spinner-border text-center" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
              )}

              {/* Modal details */}
              {openModal && (
                <ModalCours
                  cours={cours}
                  onClose={setOpenModal}
                  savedCour={savedCour}
                >
                </ModalCours>
              )}

              {!loading && (
                singleCours?.length !== 0 && (
                  singleCours?.filter(cour => cour.titre.toLowerCase().includes(searchData.toLowerCase())).length === 0 && (
                    <div
                      style={{
                        width: '100%',
                        height: 400,
                        backgroundColor: 'white',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                        flexDirection: 'column'
                      }}
                    >
                      <BsEmojiExpressionless size={82} color={'orange'} />
                      <h4>Pas de cours correspondant</h4>
                    </div>
                  )
                )
              )}

              {
                !loading && (
                  singleCours?.length === 0 && (
                    <AfficheMesage
                      error="Auccun cours ne vous a ete attribuer"
                    ></AfficheMesage>
                  )
                )
              }

            </div>

            <div className="aside-containt">
              <div className='p-0'>
                <span className="apprenant-domaine-btn">
                  {auth?.length !== 0 ? `${auth[0]?.domaine.toUpperCase()}` : 'loading...'}
                </span>
                <div>
                  {/* <p>Formations</p> */}
                  <div>
                    <div style={{
                      display: 'flex',
                      justifyContent: 'space-around',
                      marginBottom: 10
                    }}>
                     
                    </div>

                  </div>
                </div>

                {/* Send a message */}
                <Messages apprenant={auth[0]} />
              </div>
            </div>
          </div>

        </div>
      </Layout>
    </>
  );
};
