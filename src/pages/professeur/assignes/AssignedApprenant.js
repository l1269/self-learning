import React, { useState } from 'react'
import AssignedLayout from '../layouts/AssignedLayout'
import ListeApprenants from '../listeApprenants/ListeApprenants'

function AssignedApprenant({ onClose, assigneCours }) {

    const [check, setCheck] = useState(false)


    return (
        <AssignedLayout title={'Assigner'} onClose={onClose}>

            <ListeApprenants assigneCours={assigneCours} assigned={true} checked={check} onChecked={(e) => setCheck(v => !v)} />

        </AssignedLayout>
    )
}

export default AssignedApprenant