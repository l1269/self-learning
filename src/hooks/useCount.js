import { useContext } from 'react'
import { CountContext } from '../context/CountProvider'

const useCount = () => {
  return useContext(CountContext)
}

export default useCount