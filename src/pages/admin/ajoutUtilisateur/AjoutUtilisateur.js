import React, { useState } from 'react'
import { addDoc, collection, serverTimestamp} from 'firebase/firestore';
import { db } from "../../../firebase/config"
import { useAuth } from '../../../hooks/useAuth';
import "./AjoutUtilisateur.css";
import { toast } from 'react-toastify';



export default function AjoutUtilisateur() {

  // const {signup, login, userCred} = useAuth();

  const userData = JSON.parse(localStorage.getItem('userCred'));


  const { signup, login } = useAuth();

  // Notification
  const notificationStudent = () => toast("Apprenant ajpute");
  const notificationProf = () => toast("Professeur ajoute");

  // State pour message d'erreur ou de réussite
  const [error, setError] = useState(null);

  // State pour les différents champs du formulaire
  const [nom, setNom] = useState("");
  const [prenom, setPrenom] = useState("");
  const [telephone, setTelephone] = useState("");
  const [statut, setStatut] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  // State pour voir s'il faut charger ou pas en fonction du click de l'utilisateur sur le bouton ajouter
  const [loading, setLoading] = useState(false);

  // regex pour voir si le champ nom et le champ prenom contiennent que des caractères de l'alphabet
  const regexNomPrenom = /^[a-zA-Z  `']+$/;

  // regex pour verifier si l'email est en format valide
  const regexEmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

  const handleSubmit = async e => {

    e.preventDefault();
    if (!regexNomPrenom.test(nom) || !regexNomPrenom.test(prenom)) {
      return setError("Remplissez correctement le nom ou le prenom !!!!");
    }

    if (!regexEmail.test(email)) {
      return setError('Adresse email invalide');
    }

    if (password.length < 6) {
      return setError('Mot de passe court (au moins 6 caracteres)');
    }

    if (statut === "") {
      return setError("Veuillez selectionnner un statut")
    }

    try {
      // vider le message d'erreur d'abord s'il y'en a un
      setError("");
      // commencer le chargement jusqu'à ce que les données soient ajouté dans firebase et firestore
      setLoading(true);

      await signup(email, password);
      // log the user again
      await login(userData.email, userData.password);
      const data = {
        nom,
        prenom,
        email,
        telephone,
        statut,
        timeStamp: serverTimestamp()
      }

      // on ajoute dans la collection "auth" les données de l'objet data
      await addDoc(collection(db, "auth"), { prenom, nom, email, status: statut })

      /*
        on ajoute dans la collection apprenant ou professeur les données de l'objet data.
        En fonction de ce que l'admin a choisi au niveau du champ statut(soit apprenant, soit professeur),
        le state statut sera remplacé par sa valeur dans le code çi-dessous.
      */
      await addDoc(collection(db, `${statut}`), data)
      // arreter le chargement après que les infos aient été ajouter dans firebase et firestore
      setLoading(false)
      if (statut === 'apprenant') {
        notificationStudent()
      } else if (statut === 'professeur') {
        notificationProf()
      }
      // vider tous les champs du formulaire
      setNom("");
      setPrenom("");
      setTelephone("");
      setStatut("");
      setEmail("");
      setPassword("");


    } catch (error) {
      setLoading(false)
      if (error.code === 'auth/invalid-email') {
        return setError(`L'adresse email est invalide !`)
      }
      if (error.code === 'auth/user-not-found') {
        return setError('Utilisateur introuvable !')
      }
      if (error.code === 'auth/wrong-password') {
        return setError('Email ou Mot de passe incorrecte')
      }
      if (error.code === "auth/email-already-in-use") {
        return setError("Adresse email déja utilisé")
      }

    }

  }

  return (

    <div style={{ maxWidth: '700px' }} className='p-0 mx-auto'>
      <form style={{ width: '100%' }} className=' p-sm-4 p-3' onSubmit={handleSubmit} >
        <h2 className='text-center py-4'>Ajouter professeur / Apprenant</h2>
        {
          error && <div className='border border-danger text-danger my-4 p-2 rounded-2 text-center'>{error}</div>
        }
        <div className="mb-4">
          <input
            type="text"
            className='input'
            placeholder="Nom"
            value={nom}
            onChange={e => setNom(e.target.value)}
            required
          />
        </div>

        <div className="mb-4">
          <input
            type="text"
            className='input'
            placeholder="Prénom"
            value={prenom}
            onChange={e => setPrenom(e.target.value)}
            required
          />
        </div>

        <div className="mb-4">
          <input

            placeholder='77 777 77 77'
            type="tel"
            className='input'
            pattern='^(77|78|70|76) [0-9]{3} [0-9]{2} [0-9]{2}'
            value={telephone}
            onChange={e => setTelephone(e.target.value)}
            required
          />

        </div>

        <div className="mb-4">
          <select
            className="select"
            value={statut}
            onChange={e => setStatut(e.target.value)}
          >
            <option value="">Selectionnez le statut</option>
            <option value="apprenant">Apprenant</option>
            <option value="professeur">Professeur</option>
          </select>
        </div>

        <div className="mb-4">
          <input
            type="email"
            className='input'
            placeholder="Email"
            value={email}
            onChange={e => setEmail(e.target.value)}
            required
          />
        </div>

        <div className="mb-4">
          <input
            type="password"
            className='input'
            placeholder="Mot de passe"
            value={password}
            onChange={e => setPassword(e.target.value)}
            required
          />
        </div>

        <button className='btn-ajouter fs-5 px-4 mb-4' type='submit'>
          {
            loading ? <div className="loading"></div> : "Ajouter"
          }
        </button>
      </form>
    </div>
  )
}
