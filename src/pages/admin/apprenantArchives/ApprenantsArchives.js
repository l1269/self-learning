import React, { useState, useEffect } from "react";
import { db } from "../../../firebase/config";
import "./ApprenantsArchives";
import {
  doc,
  collection,
  onSnapshot,
  updateDoc,
} from "firebase/firestore";
import AfficheMesage from "../../../components/afficheMessage/AfficheMesage";
import { TiUserDeleteOutline } from "react-icons/ti";
import { toast } from "react-toastify";
import { Link } from "react-router-dom";
import Pagination from "../../../components/pagination/Pagination";

const ApprenantsArchives = () => {
  const [loading, setLoading] = useState(true);
  const [apprenants, setApprenants] = useState([]);

  // Gestion de pagination
  const [currentItems, setCurrentItems] = useState(null);
  const [pageCount, setPageCount] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);

  // Number items perPage
  const number = 10

  // Notification
  const notification = () => toast("Apprenant desarchive");

  useEffect(() => {
    const getapprenants = async () => {
      onSnapshot(collection(db, "apprenant"), (querySnapshot) => {
        const usersInfo = [];
        querySnapshot.forEach((doc) => {
          usersInfo.push({ ...doc.data(), id: doc.id });
          setApprenants([...usersInfo]);
        });
        setLoading(false);
      });
    };
    getapprenants();
  }, []);

  // Pagination functions
  useEffect(() => {
    const endOffset = itemOffset + number;
    setCurrentItems(apprenants?.filter(apprenant => apprenant.isArchiver === true).slice(itemOffset, endOffset));
    setPageCount(Math.ceil(apprenants?.filter(apprenant => apprenant.isArchiver === true).length / number));
  }, [itemOffset, apprenants, number]);

  const handlePageClick = (event) => {
    const newOffset = (event.selected * number) % apprenants?.length;
    setItemOffset(newOffset);
  };

  const desarchiver = async (id) => {
    notification();
    await updateDoc(doc(db, "apprenant", id), {
      isArchiver: false
    });
  };

  return (
    <div>
      <h4 className="mb-3 title">Apprenants Archives</h4>
      {!loading ? (
        apprenants?.filter(apprenant => apprenant.isArchiver === true).length !== 0 ? (
          <>
            <table className="table ">
              <thead>
                <tr className="text-center">
                  <th>Prenom</th>
                  <th>Nom</th>
                  <th>téléphone</th>
                  <th>domaine</th>
                  <th>Email</th>
                  <th>Password</th>
                  <th>Désarchiver</th>
                </tr>
              </thead>
              <tbody>
                {currentItems?.map((apprenant) => (
                  <tr key={apprenant.id}>
                    <td>{apprenant.prenom}</td>
                    <td>{apprenant.nom}</td>
                    <td>{apprenant.telephone}</td>
                    <td>
                      <span
                        style={{
                          padding: '4px 15px',
                          borderRadius: 5,
                          color: '#fff',
                          backgroundColor: apprenant.domaine === 'programmation' ? '#03045E' : apprenant.domaine === 'design' ? '#F48C06' : apprenant.domaine === 'marketing' ? '#005F73' : '#6A040F'
                        }}
                      >{apprenant.domaine}</span>
                    </td>
                    <td>{apprenant.email}</td>
                    <td>{apprenant.password}</td>
                    <td>
                      <TiUserDeleteOutline
                        style={{ cursor: "pointer" }}
                        className="btn-archiveL"
                        size={30}
                        onClick={() => {
                          desarchiver(apprenant.id);
                        }}
                      />
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
            {
              apprenants?.filter((apprenant) => apprenant.isArchiver === false).length > number && <Pagination pageCount={pageCount} handlePageClick={handlePageClick} />
            }
          </>
        ) : (
          <AfficheMesage
            error="Pas encore d'apprenants archivés"
            message="Archiver un apprenant"
          >
            <Link
              to="/admin/list-apprenants"
              className="link-white btn-ajouter"
              style={{ maxWidth: 200, backgroundColor: 'red' }}
            >
              Archiver apprenant
            </Link>
          </AfficheMesage>
        )
      ) : (
        <div className="text-center mt-5">
          <div className="spinner-border" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        </div>
      )}
    </div>
  );
};
export default ApprenantsArchives;
