import React, { useEffect, useState } from 'react';
import { db } from '../../../firebase/config'
import { collection, onSnapshot, doc, query, updateDoc } from 'firebase/firestore';
import AfficheMesage from '../../../components/afficheMessage/AfficheMesage'
import { TiUserDeleteOutline } from 'react-icons/ti'
import Table from 'react-bootstrap/Table';
import { toast } from 'react-toastify';
// import Profile from '../../../components/profile/Profile';
import { Link } from 'react-router-dom';
import Pagination from '../../../components/pagination/Pagination';


const ProfsArchives = () => {
  const [loading, setLoading] = useState(true);
  const [profs, setProfs] = useState([]);

  const [disabled, setDisabled] = useState(false)

  // Gestion de pagination
  const [currentItems, setCurrentItems] = useState(null);
  const [pageCount, setPageCount] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);

  // Number cours perPage
  const number = 9

  // Notification
  const notification = (msg) => toast("Professeur désarchivé");


  const [vide, setVide] = useState(true)

  useEffect(() => {
    const getProfs = async () => {
      const q = query(collection(db, "professeur"));
      onSnapshot(q, (querySnapshot) => {
        const usersInfo = [];
        querySnapshot.forEach((doc) => {
          usersInfo.push({ ...doc.data(), id: doc.id });
          setProfs([...usersInfo]);
        });
        setLoading(false);
      });
    };

    getProfs();
  }, []);

  // Pagination functions
  useEffect(() => {
    const endOffset = itemOffset + number;
    setCurrentItems(profs?.filter(prof => prof.isArchiver === true).slice(itemOffset, endOffset));
    setPageCount(Math.ceil(profs?.filter(prof => prof.isArchiver === true).length / number));
  }, [itemOffset, profs, number]);

  const handlePageClick = (event) => {
    const newOffset = (event.selected * number) % profs?.length;
    setItemOffset(newOffset);
  };

  const desarchiver = async (id) => {
    setDisabled(true)
    const selectedProf = profs.filter((prof) => prof.id === id);

    notification();

    await updateDoc(doc(db, 'professeur', `${selectedProf[0].id}`), {
      isArchiver: false
    })
    setDisabled(false)
  };

  return (
    <div>
      <h4 className="mb-3 title">Professeurs archives</h4>
      {
        !loading ?
          profs?.length !== 0 && profs?.filter(prof => prof.isArchiver === true).length !== 0 ?
            <>
              <Table responsive="sm" className='noWrap'>
                <thead>
                  <tr className='text-center'>
                    <th>Prenom</th>
                    <th>Nom</th>
                    <th>Telephone</th>
                    <th>Domaine</th>
                    <th>Email</th>
                    <th>Password</th>
                    <th>Désarchiver</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    currentItems?.map((prof) => (
                      <tr key={prof.id}>
                        <td>
                          {prof.prenom}
                        </td>
                        <td>{prof.nom}</td>
                        <td>{prof.telephone}</td>
                        <td>
                          <span
                            style={{
                              padding: '4px 15px',
                              borderRadius: 5,
                              color: '#fff',
                              backgroundColor: prof.domaine === 'programmation' ? '#03045E' : prof.domaine === 'design' ? '#F48C06' : prof.domaine === 'marketing' ? '#005F73' : '#6A040F'
                            }}
                          >{prof.domaine}</span>
                        </td>
                        <td>{prof.email}</td>
                        <td>{prof.password}</td>
                        <td>
                          {
                            disabled ? (
                              <TiUserDeleteOutline style={{ cursor: 'pointer' }} className='btn-archiveL btn-outline-green' size={30} />
                            ) : (
                              <TiUserDeleteOutline style={{ cursor: 'pointer' }}
                                onClick={() => {
                                  desarchiver(prof.id);
                                  if (profs.length === 1) {
                                    return setTimeout(() => {
                                      setVide(false)
                                    }, 500);
                                  }

                                }}
                                className='btn-archiveL btn-outline-green' size={30} />
                            )
                          }
                        </td>
                      </tr>
                    ))
                  }
                </tbody>
              </Table>
              {
                profs?.filter((prof) => prof.isArchiver === true).length > number && <Pagination pageCount={pageCount} handlePageClick={handlePageClick} />
              }
            </>
            :
            <AfficheMesage error="Pas encore de professeurs archivés" message="Archiver un professeur"  >
              <Link to="/admin/list-profs" className="link-white btn-ajouter" style={{ maxWidth: 200, backgroundColor: 'red' }}>
                Archiver Professeur
              </Link>
            </AfficheMesage>
          :
          <div className="text-center mt-5">
            <div className="spinner-border" role="status">
              <span className="visually-hidden">Loading...</span>
            </div>
          </div>
      }
    </div>
  );
};

export default ProfsArchives;
