import { FiList } from 'react-icons/fi'
import { BiArchiveIn } from 'react-icons/bi'
import { CgUserList } from 'react-icons/cg'
import { BsFileEarmarkPersonFill } from 'react-icons/bs'
import { BsFilePerson } from 'react-icons/bs'

export const links = [
    {
        label: "Liste Cours",
        icon: FiList,
        to: '',
    },
    {
        label: "Cours Archivés",
        icon: BiArchiveIn,
        to: 'cours-archives',
    },
    {
        label: "List Profs",
        icon: CgUserList,
        to: 'list-profs',
    },
    {
        label: "Profs Archivés",
        icon: BsFileEarmarkPersonFill,
        to: 'profs-archives',
    },
    {
        label: "Liste Apprenants",
        icon: CgUserList,
        to: 'list-apprenants',
    },
    {
        label: "Apprenants Archivés",
        icon: BsFilePerson,
        to: 'apprenants-archives',
    },
]