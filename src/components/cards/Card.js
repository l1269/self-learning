import React from "react";
import './Card.css'

function Card({ titre, domaine, loading, img, children, showMe }) {
  return (
    <div className="p-0 shadow">
      <div className="grid-item shadow p-0">
        <div style={{cursor: showMe ? 'pointer' : ''}} className="card-img" onClick={showMe}>
          <img className={loading ? 'skeleton': ''} src={`${img}`} alt={titre} />
          <span>{domaine}</span>
        </div>
        <div className="card-info">
          <h6 className="card-title titre-carte mt-1">{titre?.length > 20 ? titre.substr(0, 20) + '...' : titre}</h6>
          <div className="children">{children}</div>
        </div>
      </div>
    </div>
  );
}

export default Card;
