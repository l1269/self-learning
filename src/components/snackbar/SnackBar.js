import React from 'react';
import {MdOutlineCheckCircleOutline} from 'react-icons/md';
import './Snackbar.css'

function SnackBar({message}) {
  return (
   <div className="snackbar">
        <MdOutlineCheckCircleOutline className='check' />
        <p className='message'>{message}</p>
   </div>
  )
}

export default SnackBar