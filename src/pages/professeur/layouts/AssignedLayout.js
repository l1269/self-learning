import React from 'react'
import { CgClose } from 'react-icons/cg'

function AssignedLayout({ onClose, children, title }) {
    return (
        <div
            style={{
                width: '100vw',
                height: '100vh',
                backgroundColor: 'rgba(0,0,0, 0.6)',
                position: 'fixed',
                top: 0,
                left: 0,
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                zIndex: 99999
            }}
        >

            <div
                style={{
                    width: 350,
                    height: 'auto',
                    backgroundColor: 'white',
                    borderRadius: 10,
                    padding: '10px 20px',
                    transition: 'all ease 9s',
                    marginTop: 70
                }}
            >
                <div>
                    <div
                        style={{
                            width: '100%',
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            marginBottom: 15
                        }}
                    >
                        <h4>{title}</h4>
                        <CgClose size={20} style={{ fontWeight: 700, cursor: 'pointer' }} onClick={onClose} />
                    </div>

                    {children}

                </div>
            </div>

        </div>
    )
}

export default AssignedLayout