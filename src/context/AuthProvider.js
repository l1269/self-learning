import { createContext, useEffect, useState } from "react";
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  onAuthStateChanged,
  signOut,
  updateCurrentUser,
} from "firebase/auth";
import { auth } from "../firebase/config";

export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [showHeader, setShowHeader] = useState(true);

  const [currentUser, setCurrentUser] = useState(null);
  const [loading, setLoading] = useState(true);

  const toggleHeader = () => {
    const header = document.querySelector("#header");
    const headerMob = document.querySelector("#headerMob");
    const div = document.createElement("div");
    div.setAttribute("id", "deleteOpacity");
    div.style.opacity = "0.99";
    div.appendChild(header);
    div.appendChild(headerMob);
    document
      .querySelector(".right-side")
      .insertBefore(div, document.querySelector(".containt"));
  };

  const toggleHeaderRemove = () => {
    const rightSide = document.querySelector(".right-side");
    const header = document.querySelector("#header");
    const headerMob = document.querySelector("#headerMob");
    const deleteOpacity = document.querySelector("#deleteOpacity");
    rightSide.insertBefore(header, document.querySelector(".containt"));
    rightSide.insertBefore(headerMob, document.querySelector(".containt"));
    deleteOpacity?.remove();
  };

  const signup = (email, password) =>
    createUserWithEmailAndPassword(auth, email, password);
  const login = (email, password) =>
    signInWithEmailAndPassword(auth, email, password);
  const logout = () => signOut(auth);
  const editCurrentUser = (currentUser) => updateCurrentUser(auth, currentUser);

  useEffect(() => {
    const unSubscribe = onAuthStateChanged(auth, (currentUser) => {
      setCurrentUser(currentUser);
      setLoading(false);
    });
    return unSubscribe;
  });

  const value = {
    signup,
    login,
    logout,
    currentUser,
    editCurrentUser,
    showHeader,
    setShowHeader,
    toggleHeader,
    toggleHeaderRemove
  };

  return (
    <AuthContext.Provider value={value}>
      {!loading && children}
    </AuthContext.Provider>
  );
};
