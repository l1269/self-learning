import React, { useEffect, useState } from "react";
import './ListCours.css';
import ModalCours from "../../../components/modalCours/ModalCours";
import AfficheMesage from "../../../components/afficheMessage/AfficheMesage";
import Card from "../../../components/cards/Card";
import { db } from "../../../firebase/config";
import {
  collection,
  doc,
  getDocs,
  onSnapshot,
  query,
  updateDoc,
  where,
} from "firebase/firestore";
import { useAuth } from "../../../hooks/useAuth";
import SkeletonCours from "../../../components/skeleton/SkeletonCours";
import { toast } from "react-toastify";
import AssignedApprenant from "../assignes/AssignedApprenant";
import { FiExternalLink } from "react-icons/fi";
import { BsEye } from "react-icons/bs";
import Button from "../../../components/button/Button";
import AjoutCoursModal from "../../admin/listCours/AjoutCoursModal";
import { BiArchive } from "react-icons/bi";
import { TbManualGearbox } from "react-icons/tb";
import ModalEditCours from "../../admin/editCours/ModalEditCours";


const ListCours = ({ domaine }) => {

  const { currentUser } = useAuth();
  const [auth, setAuth] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const [loading, setLoading] = useState(true);
  const [cours, setCours] = useState([]);
  const [savedCour, setSavedCour] = useState(null);
  const [listCours, setListCours] = useState([]);
  const [disabled, setDisabled] = useState(false)
  const [showAssigne, setShowAssige] = useState(false)
  const [assigneCours, setAssigneCours] = useState({})
  const { toggleHeader, toggleHeaderRemove } = useAuth();

  const [addCourseModal, setAddCourseModal] = useState(false)
  const coursCollectionRef = collection(db, "cours");
  const notification = () => toast("Cours archive");

  //fonction cours

  useEffect(() => {
    const getCours = async () => {
      onSnapshot(coursCollectionRef, (querySnapshot) => {
        const usersInfo = [];
        querySnapshot.forEach((doc) => {
          usersInfo.push({ ...doc.data(), id: doc.id });
          setCours([...usersInfo]);
          setListCours([...usersInfo]);
        });
      });
    };

    const getAuth = async () => {
      const q = query(
        collection(db, "professeur"),
        where("email", "==", currentUser.email)
      );
      const querySnapshot = await getDocs(q);
      querySnapshot.forEach((doc) => {
        setAuth(
          querySnapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }))
        );
      });
      setLoading(false)
    };
    getAuth();
    getCours();

  }, []);

  useEffect(() => {
    if (showAssigne || openModal || showModal || addCourseModal) {
      toggleHeader()
    }
    else {
      toggleHeaderRemove()
    }
  }, [showAssigne, openModal, showModal, addCourseModal]);

  const archiver = async (id) => {
    setDisabled(true);
    await updateDoc(doc(db, "cours", id), {
      isArchiver: true,
      archiverPar: auth[0]?.domaine
    });
    notification();
    setOpenModal(false);
    setDisabled(false);
  }

  const handleCours = (cour) => {
    setSavedCour(cour);
    setOpenModal(true);
  };

  const handleShow = () => {
    setOpenModal(false);
    setShowModal(true);
  };

  return (
    <>
      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', flexWrap: 'wrap' }}>
        <h4 className="title">Liste des cours</h4>
        {/* Le button d'ajout de cours */}
        {listCours.filter((cour) => cour.isArchiver === false).length !== 0 && (
          <Button handleClick={() => setAddCourseModal(true)} title={"nouveau cours"} />
        )}
        <span className="profDomaine">{auth[0]?.domaine ? auth[0]?.domaine : 'loading...'}</span>
      </div>
      <div
        className="filter add-course"
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginBottom: 30,
          flexWrap: "wrap",
        }}
      >
      </div>
      <div className="grid-container ">
        {!loading ? (
          listCours.filter(cour => cour.domaine === auth[0]?.domaine).filter((cour) => cour.isArchiver === false).length !== 0 &&
          listCours?.filter(cour => cour.domaine === auth[0]?.domaine).filter((cour) => cour.isArchiver === false).map((cour) => (
            <Card
              key={cour.id}
              img={cour.img}
              titre={cour.titre}
              domaine={cour.domaine}
              duree={cour.duree}
              showMe={() => {
                setSavedCour(cour);
                setOpenModal(true);
              }}
            >
              <button
                className="card-btn"
                style={{ marginRight: 10 }}
                onClick={() => handleCours(cour)}
              >
                <BsEye
                  style={{ fontWeight: 700, fontSize: 22 }}
                />
              </button>

              <button disabled={disabled} onClick={() => archiver(cour?.id)} className='card-btn cardBtn-btn'
                style={{
                  marginRight: 10,
                }}
              >
                <BiArchive style={{ fontWeight: 700, fontSize: 22 }} />
              </button>

              <button
                className='card-btn'
                onClick={() => {
                  setShowAssige(true)
                  setAssigneCours(cour)
                }}
                style={{ marginRight: 10, backgroundColor: '#04BF55' }}
              ><TbManualGearbox style={{ fontWeight: 700, fontSize: 22 }} /></button>

              <a href={cour.url} target="_blank" rel="noreferrer" style={{ border: '1px solid #fff', borderRadius: 5, padding: '4px 5px' }}>
                <FiExternalLink color='#fff' size={22} />
              </a>

            </Card>
          )
          )
        ) : (
          [1, 2, 3, 4, 5].map(n => <SkeletonCours key={n} />)
        )}
        {openModal && (
          <ModalCours
            setShowModal={setShowModal}
            cours={cours}
            onClose={setOpenModal}
            savedCour={savedCour}
          >
            <button className="modalBouttonEdit" onClick={handleShow}>
              Modifier
            </button>
          </ModalCours>
        )}

        {showModal && (
          <ModalEditCours
            cour={savedCour}
            onCloseModal={() => setShowModal(false)}
          />
        )}

        {addCourseModal && (
          <AjoutCoursModal
            setAddCourseModal={setAddCourseModal}
            addCourseModal={addCourseModal}
          />
        )}
      </div>

      {/* Message if cours are empty */}
      {!loading && (
        listCours.filter(cour => cour.domaine === auth[0]?.domaine).filter((cour) => cour.isArchiver === false).length === 0 && (
          <AfficheMesage
            error="Pas de cours pour le moment"
          ></AfficheMesage>
        ))}

      {
        showAssigne && <AssignedApprenant assigneCours={assigneCours} onClose={() => setShowAssige(false)} />
      }
    </>
  );
};
export default ListCours;