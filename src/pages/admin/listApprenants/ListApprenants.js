import React, { useEffect, useState } from "react";
import { db } from "../../../firebase/config";
import {
  collection,
  onSnapshot,
  doc,
  updateDoc,
} from "firebase/firestore";
import "./ListApprenant.css";
import AfficheMesage from "../../../components/afficheMessage/AfficheMesage";
import { AiOutlineEdit } from "react-icons/ai";
import Table from "react-bootstrap/Table";
import EditUtilisateur from "../editUtilisateur/EditUtilisateur";
import AjoutUtilisateurModal from "../ajoutUtilisateur/AjoutUtilisateurModal";
import { toast } from "react-toastify";
import Button from "../../../components/button/Button";
import { BiArchive } from "react-icons/bi";
import { useAuth } from "../../../hooks/useAuth";
import Pagination from "../../../components/pagination/Pagination";

const ListApprenants = () => {
  const [showUserModal, setShowUserModal] = useState(false);
  const { toggleHeader, toggleHeaderRemove } = useAuth();

  const [savedApprenant, setSavedApprenant] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [loading, setLoading] = useState(true);
  const [apprenants, setApprenants] = useState([]);

  // Gestion de pagination
  const [currentItems, setCurrentItems] = useState(null);
  const [pageCount, setPageCount] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);

  // Number items perPage
  const number = 5

  // Notification
  const notification = () => toast("Apprenant archive");

  useEffect(() => {
    const getapprenants = () => {
      // const q = query(collection(db, "apprenant"), orderBy(" timeStamp", "desc"));
      onSnapshot(collection(db, "apprenant"), (querySnapshot) => {
        const usersInfo = [];
        querySnapshot.forEach((doc) => {
          usersInfo.push({ ...doc.data(), id: doc.id });
          setApprenants([...usersInfo]);
        });
        setLoading(false);
      });
    };
    getapprenants();
  }, []);

  // Pagination functions
  useEffect(() => {
    const endOffset = itemOffset + number;
    setCurrentItems(apprenants?.filter(apprenant => apprenant.isArchiver === false).slice(itemOffset, endOffset));
    setPageCount(Math.ceil(apprenants?.filter(apprenant => apprenant.isArchiver === false).length / number));
  }, [itemOffset, apprenants, number]);

  const handlePageClick = (event) => {
    const newOffset = (event.selected * number) % apprenants?.length;
    setItemOffset(newOffset);
  };

  useEffect(() => {
    if (showModal || showUserModal) {
      toggleHeader()
    }
    else {
      toggleHeaderRemove()
    }
  }, [showModal, showUserModal]);

  const handleDisplay = (apprenant) => {
    setSavedApprenant(apprenant);

    setShowModal(!showModal);
  };

  // archivage des apprenants
  const archiveApprenant = async (id) => {

    notification();
    await updateDoc(doc(db, "apprenant", id), {
      isArchiver: true
    });
  };


  return (
    <>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginBottom: 30,
          flexWrap: "wrap",
        }}
      >
        <h4 className="mb-3 title">Liste des apprenants</h4>

        {/* Button d'ajout d'utilisateur */}

        {currentItems?.filter(apprenant => apprenant.isArchiver === false).length !== 0 && (
          <Button
            title={"nouveau apprenant"}
            handleClick={() => setShowUserModal(true)}
          />
        )}
      </div>
      {!loading ? (
        apprenants?.filter(apprenant => apprenant.isArchiver === false).length !== 0 ? (
          // <div className="table-responsive">
          <Table className="table table-hover noWrap">
            <thead>
              <tr style={{ textAlign: "center" }}>
                <th scope="col">Prenom</th>
                <th scope="col">Nom</th>
                <th scope="col">téléphone</th>
                <th scope="col">domaine</th>
                <th scope="col">Email</th>
                <th scope="col">password</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
              {currentItems?.map((apprenant) => (
                <tr key={apprenant.id} className={apprenant.coach === '' ? 'missing-teacher' : undefined} title={apprenant.coach === '' ? 'professeur non assigné' : undefined}>
                  <td>{apprenant.prenom}</td>
                  <td>{apprenant.nom}</td>
                  <td>{apprenant.telephone}</td>
                  <td>
                    <span
                      style={{
                        padding: '4px 15px',
                        borderRadius: 5,
                        color: '#fff',
                        backgroundColor: apprenant.domaine === 'programmation' ? '#03045E' : apprenant.domaine === 'design' ? '#F48C06' : apprenant.domaine === 'marketing' ? '#005F73' : '#6A040F'
                      }}
                    >{apprenant.domaine}</span>
                  </td>
                  <td>{apprenant.email}</td>
                  <td>{apprenant.password}</td>
                  <td>
                    <AiOutlineEdit
                      style={{
                        cursor: "pointer",
                        color: "#ffff",
                        backgroundColor: "rgba(0,0,255, 0.4)",
                        padding: 5,
                        marginLeft: 5,
                      }}
                      className="btn-modifl"
                      size={27}
                      onClick={() => handleDisplay(apprenant)}
                    />
                    <BiArchive
                      style={{
                        cursor: "pointer",
                        color: "#ffff",
                        backgroundColor: "rgba(255,0,0, 0.4)",
                        padding: 5,
                        marginLeft: 10,
                      }}
                      className="btn-modifl"
                      size={27}
                      onClick={() => {
                        archiveApprenant(apprenant.id);
                      }}
                    />
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          // </div>
          <AfficheMesage
            message="Ajouter apprenant"
            error="Pas d'apprenants pour le moment"
          >
            <button
              onClick={() => setShowUserModal(true)}
              className="btn-ajouter"
              style={{ maxWidth: 200, backgroundColor: 'red' }}
            >
              Ajouter apprenant
            </button>
          </AfficheMesage>
        )
      ) : (
        <div className="text-center mt-5">
          <div className="spinner-border" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        </div>
      )}

      {
        apprenants?.filter((apprenant) => apprenant.isArchiver === false).length > number && <Pagination pageCount={pageCount} handlePageClick={handlePageClick} />
      }
      {showModal && (
        <EditUtilisateur
          showModal={showModal}
          setShowModal={setShowModal}
          savedUser={savedApprenant}
          user="Apprenant"
        />
      )}
      {showUserModal && (
        <AjoutUtilisateurModal
          showUserModal={showUserModal}
          setShowUserModal={setShowUserModal}
          statut="apprenant"
        />
      )}
    </>
  );
};
export default ListApprenants;
