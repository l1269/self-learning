import React, { useState } from 'react'
import { FaBars } from 'react-icons/fa';
import { AiOutlineClose } from 'react-icons/ai'
import NavLinke from '../../../components/SideBar/NavLinke'

const SidebarMob = ({links}) => {
    const [showSidebar, setShowSidebar] = useState(false)
    const closeSidebar = () => setShowSidebar(!showSidebar);
    const hideSidebar = () => setShowSidebar(false)

  return (
    <div style={{boxShadow: 'rgba(0, 0, 0, 0.3) 0px 0px 0px 3px !important'}} className='d-sm-none d-block mt-5 shadow'>
        <button onClick={closeSidebar} className='btn-hover' style={{
            backgroundColor: '#100F48',
            padding: '0.5rem 0.9rem',
            position: 'fixed',
            bottom: '5%',
            right: '10%',
            fontSize: '1.5rem',
            border: 'none',
            borderRadius: '50px',
            zIndex: '1000',
            boxShadow: 'rgba(3, 102, 214, 0.3) 0px 0px 0px 3px !important'
            }}>
                {
                    showSidebar ? <AiOutlineClose style={{ color: 'white' }} /> : <FaBars style={{ color: 'white' }} />
                }
            </button>
            {
                showSidebar && (
                    <nav style={{ zIndex: '99'}} className='vh-100 position-fixed w-95 bg-light shadow text-start pt-3'>
                        <ul className='p-0'>
                            {
                                links.map((link, key) => (
                                    <NavLinke
                                        key={key}
                                        label={link.label}
                                        Icon={link.icon}
                                        to={link.to}
                                        hendleClick={hideSidebar}
                                    />
                                ))
                            }
                        </ul>
                    </nav>
                )}
        </div>
    )
}

export default SidebarMob