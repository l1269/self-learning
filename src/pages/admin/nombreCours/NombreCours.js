import React, { useState, useEffect } from 'react'
import './NombreCours.css'
import { db } from '../../../firebase/config'
import { collection, onSnapshot, query, where } from 'firebase/firestore'
import Stat from '../stats/Stat'
import { TbBuilding, TbBusinessplan, TbCode, TbCodePlus, TbPalette } from "react-icons/tb"

export default function NombreCours() {

    const [numberCoursProg, setNumberCoursProg] = useState()
    const [numberCoursDesign, setNumberCoursDesign] = useState()
    const [numberCoursMarketing, setNumberCoursMarketing] = useState()
    const [numberCoursEntrepr, setNumberCoursEntrepr] = useState()
    const colRef = collection(db, 'cours')
    const qProg = query(colRef, where('domaine', '==', 'programmation'))
    const qDesign = query(colRef, where('domaine', '==', 'design'))
    const qMarketing = query(colRef, where('domaine', '==', 'marketing'))
    const qEntrepre = query(colRef, where('domaine', '==', 'entrepreneuriat'))

    useEffect(() => {
        const getNumberCoursProgrammation = () => {
            onSnapshot(qProg, (querySnapshot) => {
                const nombreCoursPro = querySnapshot.size
                setNumberCoursProg(nombreCoursPro)
            })
        }
        const getNumberCoursDesign = () => {
            onSnapshot(qDesign, (querySnapshot) => {
                const nombreCoursDesign = querySnapshot.size
                setNumberCoursDesign(nombreCoursDesign)
            })
        }
        const getNumberCoursMarketing = () => {
            onSnapshot(qMarketing, (querySnapshot) => {
                const nombreCoursMarketing = querySnapshot.size
                setNumberCoursMarketing(nombreCoursMarketing)
            })
        }
        const getNumberCoursEntrepreneuriat = () => {
            onSnapshot(qEntrepre, (querySnapshot) => {
                const nombreCoursEntrepr = querySnapshot.size
                setNumberCoursEntrepr(nombreCoursEntrepr)
            })
        }



        getNumberCoursProgrammation()
        getNumberCoursDesign()
        getNumberCoursMarketing()
        getNumberCoursEntrepreneuriat()

    },[])
    



    const max = '100%'
    const styles = {
        title: {
            width: max,
            textAlign: 'right',
            fontSize: 15,
            fontWeight: 500,
            textTransform: 'uppercase',
            marginBottom: -15,
        },

        info: {
            fontSize: 30,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
        },

        number: {
            fontSize: 20,
            fontWeight: 500,
        },
    }


    return (
        <div className='contenant'>

            {/* {
                data.map((item, key) => (
                    <Stat key={key} {...item} />
                ))
            } */}

            <div className='Container-course progra' style={{ borderLeft: '7px solid #6A040F' }}>
                <span className='Nprogra'></span>
                <div style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%', padding: 10 }}>
                    <p style={styles.title}>Programmation</p>
                    <hr />
                    <div style={styles.info}>
                        <TbCode />
                        <span style={styles.number}>{numberCoursProg}</span>
                    </div>

                </div>
            </div>
            <div className='Container-course design' style={{ borderLeft: '7px solid #284B63' }}>
                <span className='Ndesign'></span>
                <div style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%', padding: 10 }}>
                    <p style={styles.title}>Design</p>
                    <hr />
                    <div style={styles.info}>
                        <TbPalette />
                        <span style={styles.number}>{numberCoursDesign}</span>
                    </div>
                </div>
            </div>

            <div className='Container-course marketing' style={{ borderLeft: '7px solid #007200' }}>
                <span className='Nmarket'></span>
                <div style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%', padding: 10 }}>
                    <p style={styles.title}>Marketing</p>
                    <hr />
                    <div style={styles.info}>
                        <TbBusinessplan />
                        <span style={styles.number}>{numberCoursMarketing}</span>
                    </div>
                </div>
            </div>
            <div className='Container-course entrepr' style={{ borderLeft: '7px solid orange' }}>
                <span className='Nentrep'></span>
                <div style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%', padding: 10 }}>
                    <p style={styles.title}>Entrepreneuriat</p>
                    <hr />
                    <div style={styles.info}>
                        <TbBuilding />
                        <span style={styles.number}>{numberCoursEntrepr}</span>
                    </div>
                </div>
            </div>

        </div>
    )
}



