import { NavLink } from "react-router-dom";
import { useEffect, useState } from "react";
import { useAuth } from "../../hooks/useAuth";
import useCount from "../../hooks/useCount";
import { db } from "../../firebase/config";
import { collection, getDocs, query, where } from "firebase/firestore";

function NavLinke({ to, Icon, label, hendleClick }) {
  const [auth, setAuth] = useState([]);
  const [profDomaine, setProfDomaine] = useState();
  const { currentUser } = useAuth();
  const {
    nombreCoursArchives,
    nombreProfsArchives,
    nombreApprenantsArchives,
    nombreCoursArchivesProgrammation,
    nombreCoursArchivesDesign,
    nombreCoursArchivesMarketing,
    nombreCoursArchivesEntreprenariat,
  } = useCount();

  useEffect(() => {
    const getAuth = async () => {
      const q = query(
        collection(db, "auth"),
        where("email", "==", currentUser.email)
      );
      const querySnapshot = await getDocs(q);
      querySnapshot.forEach((doc) => {
        setAuth(
          querySnapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }))
        );
      });

      const q2 = query(
        collection(db, "professeur"),
        where("email", "==", currentUser.email)
      );
      const querySnapshot2 = await getDocs(q2);
      querySnapshot.forEach((doc) => {
        setProfDomaine(
          querySnapshot2.docs.map((doc) => ({ id: doc.id, ...doc.data() }))
        );
      });
    };
    getAuth();
  }, []);

  return (
    <li className="nav-item position-relative">
      <NavLink
        style={{ padding: "0.5rem" }}
        end
        to={`${to}`}
        className={({ isActive }) =>
          isActive ? "stretched-link active" : "stretched-link"
        }
        onClick={hendleClick ? hendleClick : null}
      >
        <Icon className="icon" />
        <span className={hendleClick ? "d-lg-inline" : "d-lg-inline d-none"}>
          {label}
        </span>
      </NavLink>

      {auth[0]?.status === "admin" &&
      nombreCoursArchives &&
      to === "cours-archives" ? (
        <span className="to-archived-cours">{nombreCoursArchives}</span>
      ) : null}
      {nombreProfsArchives && to === "profs-archives" ? (
        <span className="to-archived-profs">{nombreProfsArchives}</span>
      ) : null}
      {nombreApprenantsArchives && to === "apprenants-archives" ? (
        <span className="to-archived-apprenants">
          {nombreApprenantsArchives}
        </span>
      ) : null}

      {auth[0]?.status === "professeur" &&
      auth[0]?.domaine === "design" &&
      nombreCoursArchivesDesign &&
      to === "cours-archives" ? (
        <span className="to-archived-cours">{nombreCoursArchivesDesign}</span>
      ) : auth[0]?.domaine === "programmation" &&
        nombreCoursArchivesProgrammation &&
        to === "cours-archives" ? (
        <span className="to-archived-cours">
          {nombreCoursArchivesProgrammation}
        </span>
      ) : auth[0]?.domaine === "marketing" &&
        nombreCoursArchivesMarketing &&
        to === "cours-archives" ? (
        <span className="to-archived-cours">
          {nombreCoursArchivesMarketing}
        </span>
      ) : auth[0]?.domaine === "entrepreneuriat" &&
      nombreCoursArchivesEntreprenariat &&
        to === "cours-archives" ? (
        <span className="to-archived-cours">
          {nombreCoursArchivesEntreprenariat}
        </span>
      ) : null}
    </li>
  );
}

export default NavLinke;
