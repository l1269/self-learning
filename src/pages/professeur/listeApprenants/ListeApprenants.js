import React, { useEffect, useState } from "react";
import {
  collection,
  doc,
  getDocs,
  onSnapshot,
  query,
  updateDoc,
  where,
} from "firebase/firestore";
import { db } from "../../../firebase/config";
import AfficheMesage from "../../../components/afficheMessage/AfficheMesage";
import { useAuth } from "../../../hooks/useAuth";
import "./ListeApprenants.css";
import { HiOutlineUserCircle } from "react-icons/hi";
import AssignedCours from "../assignes/AssignedCours";
import { CgLink } from "react-icons/cg";
import ImageUpload from "../../../components/files/ImageUpload";

function AddId({ checked, apprenant, assigneCours }) {
  const onChecked = () => {
    const docApprenantRef = doc(db, "apprenant", apprenant.id);
    const docCoursRef = doc(db, "cours", assigneCours.id);

    const assigneIdApprenant = apprenant.assigne
    const assigneIdCours = assigneCours.assigne
    if (!checked) {
      if (!apprenant.assigne.find(el => el === assigneCours.id)) {
        // cours update
        assigneIdCours.push(apprenant.id)
        updateDoc(docCoursRef, {
          assigne: assigneIdCours
        })

        // apprenant update
        assigneIdApprenant.push(assigneCours.id)
        updateDoc(docApprenantRef, {
          assigne: assigneIdApprenant
        })

        checked = true
      }
    } else {
      const indexIdApprenantAssigned = assigneIdApprenant.indexOf(apprenant.assigne.find(el => el === assigneCours.id))
      const indexIdCoursAssigned = assigneIdCours.indexOf(assigneCours.assigne.find(el => el === apprenant.id))
      if (indexIdApprenantAssigned !== -1) {
        assigneIdApprenant.splice(indexIdApprenantAssigned, 1);
        assigneIdCours.splice(indexIdCoursAssigned, 1);

        // apprenant update
        updateDoc(docApprenantRef, {
          assigne: assigneIdApprenant
        })

        // cours update
        updateDoc(docCoursRef, {
          assigne: assigneIdCours
        })

        checked = false
      }
    }
  }

  return (
    <input style={{ width: 30 }} checked={checked} type={"checkbox"} onChange={onChecked} />
  )
}

const ListeApprenants = ({ assigned, children, assigneCours, professeur }) => {
  const { currentUser } = useAuth();
  const [apprenants, setApprenants] = useState([]);
  const [apprenant, setApprenant] = useState([]);
  const [loading, setLoading] = useState(true);
  const [auth, setAuth] = useState([]);
  const [coursAssigne, setCoursAssigne] = useState(false)

  useEffect(() => {
    const getapprenants = () => {
      // const q = query(collection(db, "apprenant"), orderBy(" timeStamp", "desc"));
      onSnapshot(collection(db, "apprenant"), (querySnapshot) => {
        const usersInfo = [];
        querySnapshot.forEach((doc) => {
          usersInfo.push({ ...doc.data(), id: doc.id });
          setApprenants([...usersInfo]);
        });
      });
    };
    const getAuth = async () => {
      const q = query(
        collection(db, "professeur"),
        where("email", "==", currentUser.email)
      );
      const querySnapshot = await getDocs(q);
      querySnapshot.forEach((doc) => {
        setAuth(
          querySnapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }))
        );
        setLoading(false);
      });
    };
    getapprenants();
    getAuth();
  }, []);

  const getApprenants = apprenants?.filter((apprenant) => apprenant?.domaine === auth[0]?.domaine)


  return (
    <>
      <div className="user-container">

        {
          !assigned && <ImageUpload professeur={professeur} />
        }

        <div>
          <div className="">
            <h5 className="mb-3 title" style={{ fontSize: 15 }}>
              {loading ? (
                <div className="loading"></div>
              ) : `Apprenants en ${auth[0]?.domaine}`
            }
            </h5>
          </div>
          {!loading ? (
            getApprenants.length !== 0 ? (
              apprenants
                .filter((apprenant) => apprenant?.domaine === auth[0]?.domaine)
                .map((apprenant) => (
                  <div key={apprenant.id} className="user-card" style={{ marginBottom: 13, display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                    <div key={apprenant.id} style={{ display: 'flex', alignItems: 'center' }}>
                      <div className="user-avatar">
                        <HiOutlineUserCircle className="user-avatar--icon " />
                      </div>
                      <div className="user-info" style={{ display: 'flex', flexWrap: 'nowrap' }}>
                        <h6 className="m-0" >
                          {apprenant.prenom + " " + apprenant.nom}
                        </h6>
                      </div>
                    </div>

                    {/* {children} */}
                    {!assigned ? (
                      <CgLink size={25}
                        style={{
                          fontWeight: 500,
                          cursor: 'pointer'
                        }}
                        color={"#ffff"}
                        onClick={() => {
                          setCoursAssigne(true)
                          setApprenant(apprenant)
                        }}
                      />
                    ) : (
                      <AddId apprenant={apprenant} checked={apprenant?.assigne?.find(el => el === assigneCours?.id) ? true : false} assigneCours={assigneCours} />
                    )}

                  </div>

                ))
            ) : (
              <div style={{ marginTop: 41 }}>
                <p>Pas d'apprenant pour le moment</p>
              </div>
            )) : (
            <div className="text-center mt-5">
              <div className="spinner-border" role="status">
                <span className="visually-hidden">Loading...</span>
              </div>
            </div>
          )}
        </div>
      </div>

      {
        coursAssigne && <AssignedCours apprenant={apprenant} onClose={() => setCoursAssigne(false)} />
      }
    </>
  );
};

export default ListeApprenants;
