import React, { useEffect, useState } from 'react'
import { db } from '../../../firebase/config'
import { collection, onSnapshot, doc, query, orderBy, updateDoc } from 'firebase/firestore'
import './ListProfs.css'
import AfficheMesage from '../../../components/afficheMessage/AfficheMesage'
import { AiOutlineEdit } from 'react-icons/ai'
import Table from 'react-bootstrap/Table'
import EditUtilisateur from '../editUtilisateur/EditUtilisateur';
import AjoutUtilisateurModal from '../ajoutUtilisateur/AjoutUtilisateurModal'
import { toast } from 'react-toastify';
import Button from '../../../components/button/Button';
// import Profile from '../../../components/profile/Profile'
import { BiArchive } from 'react-icons/bi'
import { useAuth } from '../../../hooks/useAuth'
import Pagination from '../../../components/pagination/Pagination'
// import ListApprenants from '../listApprenants/ListApprenants'

const ListProfs = () => {
  const { toggleHeader, toggleHeaderRemove } = useAuth();
  const [showUserModal, setShowUserModal] = useState(false);

  const [savedProfesseur, setSavedProfesseur] = useState(null);
  const [showModal, setShowModal] = useState(false);

  const [loading, setLoading] = useState(true);
  const [profs, setProfs] = useState([]);

  // Gestion de pagination
  const [currentItems, setCurrentItems] = useState(null);
  const [pageCount, setPageCount] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);

  // Number items perPage
  const number = 5

  // Notification
  const notification = () => toast("Professeur archive");


  useEffect(() => {

    const getProfs = async () => {
      const q = query(collection(db, "professeur"), orderBy("timeStamp", "desc"));
      onSnapshot(q, (querySnapshot) => {

        const usersInfo = [];
        querySnapshot.forEach((doc) => {
          usersInfo.push({ ...doc.data(), id: doc.id });
          setProfs([...usersInfo]);

        });
        setLoading(false);
      });

    }
    getProfs();

  }, [])

  useEffect(() => {
    if (showModal || showUserModal) {
      toggleHeader()
    }
    else {
      toggleHeaderRemove()
    }
  }, [showModal, showUserModal]);

  // Pagination functions
  useEffect(() => {
    const endOffset = itemOffset + number;
    setCurrentItems(profs?.filter(prof => prof.isArchiver === false).slice(itemOffset, endOffset));
    setPageCount(Math.ceil(profs?.filter(prof => prof.isArchiver === false).length / number));
  }, [itemOffset, profs, number]);

  const handlePageClick = (event) => {
    const newOffset = (event.selected * number) % profs?.length;
    setItemOffset(newOffset);
  };

  const handleDisplay = (professeur) => {
    setSavedProfesseur(professeur);
    setShowModal(!showModal);
  }

  const Archiver = async (id) => {
    const selectedProf = profs?.filter(prof => prof.id === id);
    notification()

    await updateDoc(doc(db, 'professeur', `${selectedProf[0].id}`), {
      isArchiver: true
    })
  }

  return (
    <>
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          marginBottom: 30,
          flexWrap: 'wrap'
        }}
      >
        <h4 className="mb-3 title">Liste des professeurs</h4>
        {/* Button d'ajout d'utilisateur */}
        {profs?.length !== 0 && <Button title={'nouveau professeur'}
          handleClick={() => setShowUserModal(true)} />}
      </div>
      {
        !loading ?
          profs?.length !== 0 && profs?.filter(prof => prof.isArchiver === false).length !== 0 ? (

            <Table responsive="sm" className='noWrap'>
              <thead>
                <tr style={{ textAlign: 'center' }}>
                  <th>Prenom</th>
                  <th>Nom</th>
                  <th>Telephone</th>
                  <th>Domaine</th>
                  <th>Email</th>
                  <th>password</th>
                  <th>Actions</th>

                </tr>
              </thead>
              <tbody>
                {
                  currentItems?.map((prof) => (
                    <tr key={prof.id}>
                      <td>
                        {prof.prenom}
                      </td>
                      <td>
                        {prof.nom}
                      </td>
                      <td>{prof.telephone}</td>
                      <td>
                        <span
                          style={{
                            padding: '4px 15px',
                            borderRadius: 5,
                            color: '#fff',
                            backgroundColor: prof.domaine === 'programmation' ? '#03045E' : prof.domaine === 'design' ? '#F48C06' : prof.domaine === 'marketing' ? '#005F73' : '#6A040F'
                          }}
                        >{prof.domaine}</span>
                      </td>
                      <td>{prof.email}</td>
                      <td>{prof.password}</td>
                      <td>
                        <AiOutlineEdit
                          style={{
                            cursor: 'pointer', color: '#ffff', backgroundColor: 'rgba(0,0,255, 0.4)', padding: 5, marginLeft: 5,
                          }}
                          className='btn-modifl' size={27} onClick={() => handleDisplay(prof)}
                        />
                        <BiArchive
                          style={{ cursor: 'pointer', color: '#ffff', backgroundColor: 'rgba(255,0,0, 0.4)', padding: 5, marginLeft: 10 }}
                          className='btn-modifl' size={27}
                          onClick={() => Archiver(prof.id)}
                        />
                      </td>
                    </tr>
                  ))
                }
              </tbody>
            </Table>
          ) : (
            <AfficheMesage
              error="Pas de professeurs pour le moment"
              message="Ajouter professeur"
            >
              <button
                onClick={() => setShowUserModal(true)}
                className="btn-ajouter"
                style={{ backgroundColor: 'red' }}
              >
                Ajouter Professeur
              </button>
            </AfficheMesage>
          ) : (
            <div className="text-center mt-5">
              <div className="spinner-border" role="status">
                <span className="visually-hidden">Loading...</span>
              </div>
            </div>
          )
      }
      {
        profs?.filter((cour) => cour.isArchiver === false).length > number && <Pagination pageCount={pageCount} handlePageClick={handlePageClick} />
      }
      {
        showModal && <EditUtilisateur showModal={showModal} setShowModal={setShowModal} savedUser={savedProfesseur} user="Professeur" />
      }
      {showUserModal && <AjoutUtilisateurModal setShowUserModal={setShowUserModal} statut="professeur" />}
    </>
  )
}
export default ListProfs