import React, { useState, useEffect } from "react";
import { db } from "../../../firebase/config";
import {
  collection,
  onSnapshot,
  doc,
  query,
  where,
  getDocs,
  updateDoc,
} from "firebase/firestore";
import AfficheMesage from "../../../components/afficheMessage/AfficheMesage";
import { toast } from "react-toastify";
import Card from "../../../components/cards/Card";
import { Link } from "react-router-dom";
import SkeletonCours from "../../../components/skeleton/SkeletonCours";
import { useAuth } from "../../../hooks/useAuth";

const CoursArchives = () => {
  const [loading, setLoading] = useState(true);
  const [listCours, setListCours] = useState([]);
  const coursCollectionRef = collection(db, "cours");
  const [auth, setAuth] = useState([]);
  const { currentUser } = useAuth();

  const [disabled, setDisabled] = useState(false);

  // Notification
  const notification = () => toast("Cours desarchive");

  // const [vide, setVide] = useState(true)

  const desArchiver = async (id) => {
    setDisabled(true);
    await updateDoc(doc(db, "cours", id), {
      isArchiver: false,
      archiverPar: null
    });
    notification();
    setDisabled(false);
  };

  useEffect(() => {
    const getCours = async () => {
      onSnapshot(coursCollectionRef, (querySnapshot) => {
        const usersInfo = [];
        querySnapshot.forEach((doc) => {
          usersInfo.push({ ...doc.data(), id: doc.id });
          setListCours([...usersInfo]);
        });
      });
    };
    const getAuth = async () => {
      const q = query(
        collection(db, "professeur"),
        where("email", "==", currentUser.email)
      );
      const querySnapshot = await getDocs(q);
      querySnapshot.forEach((doc) => {
        setAuth(
          querySnapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }))
        );
      });
      setLoading(false)
    };

    getCours();
    getAuth();
  }, []);

  return (
    <>
      <h4 className="mb-3 title">Cours archivés</h4>
      <div
        className="grid-container "
        style={{
          marginTop: 30,
        }}
      >
        {!loading ? (
          listCours.filter(cour => cour.domaine === auth[0]?.domaine).filter((cour) => cour.isArchiver === true).length !== 0 && (
            listCours.filter(cour => cour.domaine === auth[0]?.domaine).filter((cour) => cour.isArchiver === true)?.map((cour) => (
              <Card
                key={cour.id}
                img={cour.img}
                titre={cour.titre}
                domaine={cour.domaine}
                duree={cour.duree}
              >
                {cour.archiverPar === auth[0]?.domaine ? (
                  <button
                    disabled={disabled}
                    onClick={() => {
                      desArchiver(cour.id)
                    }}
                    className="button-listcour modalBtn"
                  >
                    Désarchivé
                  </button>
                ) : null}
              </Card>
            ))
          )
        ) : (
          [1, 2, 3, 4, 5].map((n) => <SkeletonCours key={n} />)
        )}
      </div>
      <div>
        {!loading && (
          listCours.filter(cour => cour.domaine === auth[0]?.domaine).filter((cour) => cour.isArchiver === true).length === 0 && (
            <AfficheMesage error="Pas de cours Archiver">
              <Link
                to="/professeur"
                className="link-white btn-ajouter"
                style={{ maxWidth: 200, backgroundColor: 'red' }}
              >
                Archiver cours
              </Link>
            </AfficheMesage>
          ))}
      </div>
    </>
  );
};

export default CoursArchives;
