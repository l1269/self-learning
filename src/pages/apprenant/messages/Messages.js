import emailjs from '@emailjs/browser';
import React, { useState } from 'react'
import { toast } from 'react-toastify';

function Messages({ apprenant }) {
    // console.log(apprenant);

    const [sujet, setSujet] = useState('')
    const [message, setMessage] = useState('')

    // Notification
    const notificationSuccess = () => toast("Votre message a ete envoyer");
    const notificationError = () => toast("Message non envoyer");

    const handleSubmit = (e) => {
        e.preventDefault();

        const options = {
            from_name: sujet,
            to_name: apprenant.prenom + ' ' + apprenant.nom,
            message: message,
        }


        try {
            emailjs.send("service_r8xizni", "template_x7r9nor", options, 'OW_da_d2vG4yI7gQQ').then((result) => {
                setSujet('')
                setMessage('')
                notificationSuccess();
            }, (error) => {
                notificationError();
            });


        } catch (err) {
            console.log(err);
        }
    }

    const styleButton = {
        all: 'unset',
        borderRadius: 7,
        padding: '5px 10px',
        backgroundColor: 'rgba(0, 23, 193, 0.5)',
        fontSize: 16,
        fontWeight: 500,
        color: '#fff',
        marginRight: 20,
        cursor: 'pointer'
    }

    return (
        <div>
            <div style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                flexWrap: 'nowrap',
                marginTop: 50
            }}>
                <span
                    style={{
                        width: '100%',
                        height: 'auto',
                        padding: '7px 10px',
                        borderRadius: 7,
                        backgroundColor: 'rgba(0, 23, 193, 0.5)',
                        fontSize: 18,
                        color: '#fff',
                        fontWeight: 500,
                        cursor: 'pointer'
                    }}
                >
                    Ecrivez nous
                </span>
            </div>
            <form style={{ marginTop: 20, backgroundColor: 'transparent' }}>
                <div className="col-12 col-sm-12 col-md-12 mb-12">
                    <input
                        type="text"
                        className="input-modal"
                        placeholder='Sujet'
                        name='nom'
                        required
                        value={sujet}
                        onChange={(e) => setSujet(e.target.value)}
                    />
                </div>
                <div className="col-12 col-md-12 mb-3">
                    <textarea
                        value={message}
                        onChange={(e) => setMessage(e.target.value)}
                        required
                        style={{ marginTop: 20, borderRadius: 7, border: '1px solid gray' }}
                        cols="30"
                        rows="8"
                        placeholder="Entrer votre message..."
                        name={"description"}
                    ></textarea>
                </div>

                <div style={{ width: '100%', display: 'flex', justifyContent: 'flex-end' }}>
                    <button
                        style={{
                            all: 'unset',
                            borderRadius: 7,
                            padding: '5px 10px',
                            backgroundColor: 'rgba(0, 23, 193, 0.5)',
                            fontSize: 16,
                            fontWeight: 500,
                            width: 200,
                            textAlign: 'center',
                            color: '#fff',
                            cursor: 'pointer'
                        }}
                        onClick={handleSubmit}
                        type='submit'>Envoyer</button>
                </div>
            </form>

        </div>
    )
}

export default Messages