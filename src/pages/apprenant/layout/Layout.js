import GetProf from './GetProf'
import './layout.css'

export const Layout = ({ children, id }) => {

  return (
    <div
      style={{
        backgroundColor: 'rgb(240,236,236)',
        width: '100%',
        height: 'auto',
        // paddingTop: '20px',
        paddingBottom: '40px',
      }}
    >
      <div
        style={{
          width: '100%', height: 300, margin: '0  auto 50px auto',
          // clipPath: 'polygon(60% 100%, 0 100%, 0 51%, 0 0, 60% 0)',
          backgroundColor: '#fff',
          display: 'flex',
          flexWrap: 'wrap',
          alignItems: 'center',
          justifyContent: 'center'
        }}
      >
        <h3
          style={{
            fontSize: 40,
            fontWeight: 900,
            padding: '0 30px'
          }}
          className='rainbow-text'
        >
          SPACE
          <br />
          APPRENANT
        </h3>

        <div
          style={{
            height: 250,
            borderLeft: '5px solid #100F48',
            padding: '10px 20px'
          }}
        >
          <div style={{
            width: 200,
            height: 200,
            borderRadius: '50%',
            overflow: 'hidden',
            marginBottom: 10
          }}>
            <img
              // src={'https://images.unsplash.com/photo-1573497019940-1c28c88b4f3e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fHVzZXJ8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60'}
              src={'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRISxBTQ88B9PvlreCwRY0_wqZK7y4XoG4zIQ&usqp=CAU'}
              alt={"prof"}
              style={{
                width: '100%',
                height: '100%',
                objectFit: 'cover'
              }}
            />
          </div>

          <GetProf id={id} />

        </div>
      </div>
      {children}
    </div>
  )
}
