import { useEffect, useState } from "react";
import { updateDoc, doc, query, collection, where, getDocs } from "firebase/firestore";
import { db, storage } from "../../../firebase/config";
import "./ModaleditCours.css";
import { FiDelete, FiUpload } from "react-icons/fi";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { getDownloadURL, ref, uploadBytesResumable } from "firebase/storage";
import { useAuth } from "../../../hooks/useAuth";

export default function EditCours({ cour, onCloseModal }) {
  // State pour message d'erreur ou de réussite
  const [error, setError] = useState(null);

  // State pour les différents champs du formulaire
  const [date, setDate] = useState(cour.date);
  const [url, setUrl] = useState(cour.url);
  const [titre, setTitre] = useState(cour.titre);
  const [description, setDescription] = useState(cour.description);
  const [domaine, setDomaine] = useState(cour.domaine);
  const [file, setFile] = useState(null);
  const [img, setImg] = useState(cour.img);
  const [per, setPerc] = useState(null);

  // State pour voir s'il faut charger ou pas en fonction du click de l'utilisateur sur le bouton ajouter
  const [loading, setLoading] = useState(false);

  const notify = () => toast("Modification reussi");

  const { currentUser } = useAuth();
  const [auth, setAuth] = useState([]);

  const getAuth = async () => {
    const q = query(
      collection(db, "auth"),
      where("email", "==", currentUser.email)
    );
    const querySnapshot = await getDocs(q);
    querySnapshot.forEach((doc) => {
      setAuth(querySnapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() })));
    });
  };
  getAuth();

  useEffect(() => {
    const uploadFile = () => {
      // const name = new Date().getTime() + file.name;

      const storageRef = ref(storage, file.name);
      const uploadTask = uploadBytesResumable(storageRef, file);

      uploadTask.on(
        "state_changed",
        (snapshot) => {
          const progress =
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          setPerc(progress);
        },
        (error) => {
          return error;
        },
        () => {
          getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
            setImg(downloadURL);
          });
        }
      );
    };
    file && uploadFile();
  }, [file]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (domaine === "") {
      return setError("Veuillez selectionnner un domaine");
    }

    try {
      // vider le message d'erreur d'abord s'il y'en a un
      setError("");

      // commencer le chargement jusqu'à ce que les données soient ajouté dans firebase et firestore
      setLoading(true);

      // old data
      const data = {
        domaine: auth[0]?.status === 'admin' ? domaine : cour.domaine,
        titre,
        date,
        url,
        description,
        img,
      };

      const { id } = cour;

      // find cours
      const docRef = doc(db, "cours", id);

      // Update cours
      await updateDoc(docRef, data);

      // arreter le chargement après que les infos aient été ajouter dans firebase et firestore
      setLoading(false);
      notify();
      // vider tous les champs du formulaire
      setDomaine("");
      setTitre("");
      setDate("");
      setUrl("");
      setDescription("");

      // close Modal
      onCloseModal(false);
    } catch (error) {
      setLoading(false);
      return setTimeout(() => {
        setError("Une erreur est survenue");
      }, 3000);
    }
  };

  const btnStyle = {
    before: {
      borderRadius: 10,
      backgroundColor: "rgba(255, 0,0, 0.5)",
      color: "white",
      fontWeight: 700,
      padding: "8px 20px",
      width: "100%",
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      cursor: "pointer",
    },
    after: {
      borderRadius: 10,
      backgroundColor: "green",
      color: "white",
      fontWeight: 700,
      padding: "8px 20px",
      width: "100%",
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      cursor: "pointer",
    },
  };

  return (
    <div className="overLayEdit">
      <div className="modalContainerEdit">
        <form
          style={{ width: "100%" }}
          onSubmit={handleSubmit}
          className="mx-sm-auto p-sm-4 p-2"
        >
          <div className="header_form_editCour">
            <h2 className="text-center py-4">Modification du cours</h2>
            <p className="closeBtnEdit" onClick={onCloseModal}>
              <FiDelete size={30} />
            </p>
          </div>
          {error && (
            <div className="border border-danger text-danger my-4 p-2 rounded-2 text-center">
              {error}
            </div>
          )}

          {/* Inputs */}
          <div className="mb-4">
            <label
              htmlFor="file"
              style={{ display: "flex", justifyContent: 'center', flexWrap: "nowrap", width: "250px", margin: '0 auto' }}
            >
              <span style={img ? btnStyle.after : btnStyle.before}>
                <FiUpload style={{ marginRight: 20, color: "#fff" }} size={20} />
                {per && per < 100 ? (
                  <div className="loading"></div>
                ) : per === 100 ? (
                  file?.name.substr(0, 30) + "..."
                ) : (
                  img.substr(0, 30) + "..."
                )}
              </span>
            </label>
            <input
              type="file"
              id="file"
              onChange={(e) => setFile(e.target.files[0])}
              style={{ display: "none" }}
            />
          </div>

          <div className="mb-4">
            {
              auth[0]?.status === 'admin' && (
                <select
                  className="input-modal"
                  value={domaine}
                  onChange={(e) => setDomaine(e.target.value)}
                  required
                >
                  <option value="">Selectionnez le domaine</option>
                  <option value="programmation">Programmation</option>
                  <option value="design">Design</option>
                  <option value="marketing">Marketing Digital</option>
                  <option value="entrepreneuriat">entrepreneuriat</option>
                </select>
              )
            }
          </div>

          <div className="mb-4">
            <input
              type="text"
              className="input-modal"
              placeholder="Titre"
              value={titre}
              onChange={(e) => setTitre(e.target.value)}
              required
            />
          </div>

          <div className="mb-4">
            <input
              type="url"
              className="input-modal"
              placeholder="Lien du cours"
              value={url}
              name={"url"}
              onChange={(e) => setUrl(e.target.value)}
              required
            />
          </div>

          <div className="mb-4">
            <input
              type="date"
              className="input-modal"
              placeholder="Durée"
              value={date}
              onChange={(e) => setDate(e.target.value)}
              required
            />
          </div>
          <div className="mb-4">
            <textarea
              className="input-modal"
              cols="30"
              rows="5"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            ></textarea>
          </div>
          <div className="text-center mb-3">
            {loading ? (
              <button className="btn-ajouter fs-5 px-4 mb-4">
                <div className="loading"></div>
              </button>
            ) : (
              <button
                disabled={per === 1000 && true}
                type="submit"
                className="btn-edit"
              >
                Modifier
              </button>
            )}
          </div>
        </form>
      </div>
    </div>
  );
}
