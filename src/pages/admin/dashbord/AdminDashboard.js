import React, { useEffect } from "react";
import ListCours from "../listCours/ListCours.js";
import { Routes, Route } from "react-router-dom";
import ListProfs from "../listProfs/ListProfs.js";
import CoursArchives from "../coursArchives/CoursArchives.js";
import ProfsArchives from "../profsArchives/ProfsArchives.js";
import ListApprenants from "../listApprenants/ListApprenants.js";
import ApprenantsArchives from "../apprenantArchives/ApprenantsArchives.js";
import SidebarMob from "../adminSidebar/SidebarMob.js";
import NombreProf from "../nombreProf/NombreProf.js";
import NombreApprenant from "../nombreApprenant/NombreApprenant.js";
import NombreCours from "../nombreCours/NombreCours.js";
import "./AdminDashboard.css";
import Header from "../../../components/header/Header.js";
import HeaderMob from "../../../components/header/HeaderMob.js";
import SideBar from "../../../components/SideBar/SideBar.js";
import { links } from "../../../utils/AdminData";
import { Navigate } from "react-router-dom";
import { useAuth } from "../../../hooks/useAuth.js";
import { CountProvider } from "../../../context/CountProvider.js";
import { resize } from "../../../utils/resize.js";
import ErrorPage from "../../../components/errorPage/ErrorPage.js";
const Dashbord = () => {

  const { currentUser } = useAuth();

  useEffect(() => {
    let width = document.getElementById('sidebar')?.offsetWidth;
    document.querySelector('.right-side').style.paddingLeft = `${width}px`;
  }, [])

  resize()



  if (!currentUser) {
    return <Navigate to={"/"}></Navigate>;
  }

  return (
    <>
      <div className="dashboard background">
        <div style={{ maxWidth: "230px" }} className="left-side backSide p-0">
          <CountProvider>
            <SideBar links={links} />
            <SidebarMob links={links} />
          </CountProvider>
        </div>
        <div className="right-side">
          <HeaderMob title="Admin Workspace" />
          <Header />
          <div className="containt">
            <div style={{ opacity: '0.99' }} className="main-containt table-responsive">
              <Routes>
                <Route index element={<ListCours />} />
                <Route path="cours-archives" element={<CoursArchives />} />
                <Route path="list-profs" element={<ListProfs />} />
                <Route path="profs-archives" element={<ProfsArchives />} />
                <Route path="list-apprenants" element={<ListApprenants />} />
                <Route
                  path="apprenants-archives"
                  element={<ApprenantsArchives />}
                />
                <Route path="*" element={<ErrorPage/>} />
              </Routes>
            </div>
            <div className="aside-containt">
              <div className="p-0">
                <NombreCours />
                <NombreProf />
                <NombreApprenant />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Dashbord;
