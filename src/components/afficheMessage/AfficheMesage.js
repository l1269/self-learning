import React from "react";
import "./AfficheMessage.css";
import { CgDanger } from "react-icons/cg";
export default function AfficheMesage(props) {
  return (

    <section className="d-flex flex-column  justify-content-center align-items-center py-5" >
      <CgDanger className="icon-danger mb-4" style={{ fontSize: "3rem" }} />
      <p className="text-center text-danger">{props.error}</p>
      {
        props.children
      }
    </section>
  );
}
