import React, { useState } from 'react';
import './notification.css'
import { BsCheckCircle, BsXCircle } from "react-icons/bs";

function Notification({ message, type }) {

    const [notif, setNotif] = useState(true)

    setTimeout(() => {
        setNotif(false)
    }, 2000)

    if (type === 'success') {
        return (
            <>
                {
                    notif && (
                        <div
                            className='notif_container'
                            style={{
                                borderLeft: `6px solid #17B293`,
                                display: 'flex',
                                alignItems: 'center',
                                transition: 'all ease 0.7s'
                            }}
                        >
                            <BsCheckCircle
                                style={{
                                    color: '#17B293',
                                    fontSize: '25px'
                                }}
                            />
                            <p>{message}</p>

                        </div>
                    )
                }
            </>
        )
    } else {
        return (
            <>
                {
                    notif && (
                        <div
                            className='notif_container'
                            style={{
                                borderLeft: `6px solid #FC8476`,
                                display: 'flex',
                                alignItems: 'center',
                            }}
                        >
                            <BsXCircle
                                style={{
                                    color: '#FC8476',
                                    fontSize: '25px'
                                }}
                            />
                            <p>{message}</p>

                        </div>
                    )
                }
            </>
        )
    }
}

export default Notification