import { collection, onSnapshot, query, where } from "firebase/firestore";
import { createContext, useEffect, useState } from "react";
import { db } from "../firebase/config";

export const CountContext = createContext();

export const CountProvider = ({ children }) => {
  const [nombreCoursArchives, setNombreCoursArchives] = useState();
  const [nombreCoursArchivesProgrammation, setNombreCoursArchivesProgrammation] = useState();
  const [nombreCoursArchivesDesign, setNombreCoursArchivesDesign] = useState();
  const [nombreCoursArchivesMarketing, setNombreCoursArchivesMarketing] = useState();
  const [nombreCoursArchivesEntreprenariat, setNombreCoursArchivesEntreprenariat] = useState();
  const [nombreProfsArchives, setNombreProfsArchives] = useState();
  const [nombreApprenantsArchives, setNombreApprenantsArchives] = useState();
  useEffect(() => {
    const getNumberCoursArchives = () => {
      onSnapshot(collection(db, "cours"), (querySnapshot) => {
        const usersInfo = [];
        querySnapshot.forEach((doc) => {
          usersInfo.push({ ...doc.data(), id: doc.id });
          setNombreCoursArchives([...usersInfo]);
        });
      });

      // Programmation
      const q1 = query(collection(db, "cours"), where("domaine", "==", "programmation"))
      onSnapshot(q1, (querySnapshot) => {
        const usersInfo = [];
        querySnapshot.forEach((doc) => {
          usersInfo.push({ ...doc.data(), id: doc.id });
          setNombreCoursArchivesProgrammation([...usersInfo]);
        });
      });
      // Design
      const q2 = query(collection(db, "cours"), where("domaine", "==", "design"))
      onSnapshot(q2, (querySnapshot) => {
        const usersInfo = [];
        querySnapshot.forEach((doc) => {
          usersInfo.push({ ...doc.data(), id: doc.id });
          setNombreCoursArchivesDesign([...usersInfo]);
        });
      });
      // Marketing
      const q3 = query(collection(db, "cours"), where("domaine", "==", "marketing"))
      onSnapshot(q3, (querySnapshot) => {
        const usersInfo = [];
        querySnapshot.forEach((doc) => {
          usersInfo.push({ ...doc.data(), id: doc.id });
          setNombreCoursArchivesMarketing([...usersInfo]);
        });
      });

      // Entrepreneuriat
      const q4 = query(collection(db, "cours"), where("domaine", "==", "entrepreneuriat"))
      onSnapshot(q4, (querySnapshot) => {
        const usersInfo = [];
        querySnapshot.forEach((doc) => {
          usersInfo.push({ ...doc.data(), id: doc.id });
          setNombreCoursArchivesEntreprenariat([...usersInfo]);
        });
      });

    };
    const getNumberProfArchives = () => {
      onSnapshot(collection(db, "professeur"), (querySnapshot) => {
        const usersInfo = [];
        querySnapshot.forEach((doc) => {
          usersInfo.push({ ...doc.data(), id: doc.id });
          setNombreProfsArchives([...usersInfo]);
        });
      });
    };
    const getNumberApprenantsArchives = () => {
      onSnapshot(collection(db, "apprenant"), (querySnapshot) => {
        const usersInfo = [];
        querySnapshot.forEach((doc) => {
          usersInfo.push({ ...doc.data(), id: doc.id });
          setNombreApprenantsArchives([...usersInfo]);
        });
      });
    };
    getNumberCoursArchives();
    getNumberProfArchives();
    getNumberApprenantsArchives();
  }, []);

  const value = {
    nombreCoursArchives: nombreCoursArchives?.filter(cour => cour.isArchiver === true).length,
    nombreProfsArchives: nombreProfsArchives?.filter(cour => cour.isArchiver === true).length,
    nombreApprenantsArchives: nombreApprenantsArchives?.filter(cour => cour.isArchiver === true).length,
    nombreCoursArchivesProgrammation: nombreCoursArchivesProgrammation?.filter(cour => cour.isArchiver === true).length,
    nombreCoursArchivesDesign: nombreCoursArchivesDesign?.filter(cour => cour.isArchiver === true).length,
    nombreCoursArchivesMarketing: nombreCoursArchivesMarketing?.filter(cour => cour.isArchiver === true).length,
    nombreCoursArchivesEntreprenariat: nombreCoursArchivesEntreprenariat?.filter(cour => cour.isArchiver === true).length
  }

  return <CountContext.Provider value={value}>{children}</CountContext.Provider>;
};
