import React from 'react'
import { RiAddCircleLine } from 'react-icons/ri'

function Button({ handleClick, title }) {

    const btnStyle = {
        backgroundColor: '#7a89fe',
        padding: '5px 10px',
        borderRadius: 7,
        color: 'white',
        fontWeight: 500,
        display: 'flex',
        alignItems: 'center',
        cursor: "pointer",
        boxShadow: 'rgba(0, 0, 0, 0.1) 0px 20px 25px -5px, rgba(0, 0, 0, 0.04) 0px 10px 10px -5px',
        marginBottom: 15,
    }

    return (
        <div
            style={btnStyle}
            onClick={handleClick}
        >
            <RiAddCircleLine
                size={30}
            />
            <span
                style={{
                    fontSize: 18,
                    marginLeft: 5
                }}
            >{title}</span>
        </div>
    )
}

export default Button