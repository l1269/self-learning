import React, { useState, useEffect } from "react";
import { db } from "../../../firebase/config";
import {
  collection,
  onSnapshot,
  doc,
  query,
  orderBy,
  updateDoc,
} from "firebase/firestore";
import FiltreCours from "../../../components/filtreCours/FiltreCours";
import AfficheMesage from "../../../components/afficheMessage/AfficheMesage";
import { toast } from "react-toastify";
import Card from "../../../components/cards/Card";
import { Link } from "react-router-dom";
import SkeletonCours from "../../../components/skeleton/SkeletonCours";
import Pagination from "../../../components/pagination/Pagination";

const CoursArchives = () => {
  const [loading, setLoading] = useState(true);
  const [cours, setCours] = useState([]);
  const [listCours, setListCours] = useState([]);
  const coursCollectionRef = collection(db, "cours");

  const [disabled, setDisabled] = useState(false);

  // Gestion de pagination
  const [currentItems, setCurrentItems] = useState(null);
  const [pageCount, setPageCount] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);

  // Number cours perPage
  const number = 9

  // Notification
  const notification = () => toast("Cours desarchive");

  const desArchiver = async (id) => {
    setDisabled(true);
    await updateDoc(doc(db, "cours", id), {
      isArchiver: false,
      archiverPar: null
    });
    notification();
    setDisabled(false);
  };

  useEffect(() => {
    const getCours = async () => {
      const q = query(coursCollectionRef, orderBy("timeStamp", "desc"));
      onSnapshot(q, (querySnapshot) => {
        const usersInfo = [];
        querySnapshot.forEach((doc) => {
          usersInfo.push({ ...doc.data(), id: doc.id });
          setCours([...usersInfo]);
          setListCours([...usersInfo]);
        });
        setLoading(false);
      });
    };

    getCours();
  }, []);

  // Pagination functions
  useEffect(() => {
    const endOffset = itemOffset + number;
    setCurrentItems(listCours?.filter((cour) => cour.isArchiver === true).slice(itemOffset, endOffset));
    setPageCount(Math.ceil(listCours?.filter((cour) => cour.isArchiver === true).length / number));
  }, [itemOffset, listCours, number]);

  const handlePageClick = (event) => {
    const newOffset = (event.selected * number) % listCours?.length;
    setItemOffset(newOffset);
  };

  return (
    <>
      <h4 className="mb-3 title">Cours archivés</h4>
      <FiltreCours cours={cours} setListCours={setListCours} />
      <div
        className="grid-container "
        style={{
          marginTop: 30,
        }}
      >
        {!loading ? (
          listCours?.filter((cour) => cour.isArchiver === true).length !== 0 && (
            currentItems?.map((cour) => (
              <Card
                key={cour.id}
                img={cour.img}
                titre={cour.titre}
                domaine={cour.domaine}
                duree={cour.duree}
              >
                <button
                  disabled={disabled}
                  onClick={() => {
                    desArchiver(cour.id);
                  }}
                  className="button-listcour modalBtn"
                >
                  Désarchivé
                </button>
              </Card>
            ))
          )
        ) : (
          [1, 2, 3, 4, 5].map((n) => <SkeletonCours key={n} />)
        )}
      </div>
      <div>
        {!loading && (
          listCours?.filter((cour) => cour.isArchiver === true).length === 0 && (
            <AfficheMesage error="Pas de cours Archiver">
              <Link
                to="/admin"
                className="link-white btn-ajouter"
                style={{ maxWidth: 200, backgroundColor: "red" }}
              >
                Archiver cours
              </Link>
            </AfficheMesage>
          ))}
      </div>

      {
        listCours?.filter((cour) => cour.isArchiver === true).length > number && <Pagination pageCount={pageCount} handlePageClick={handlePageClick} />
      }
    </>
  );
};

export default CoursArchives;
