import ReactPaginate from 'react-paginate'
import './pagination.css'

function Pagination({ pageCount, handlePageClick }) {

    return (
        <ReactPaginate
            // breakLabel="..."
            nextLabel=">>"
            onPageChange={handlePageClick}
            pageRangeDisplayed={5}
            pageCount={pageCount}
            previousLabel="<<"
            renderOnZeroPageCount={null}
            className={'pagination'}
            activeClassName={'pagination-active'}
            previousClassName={'pagination-prev'}
            nextClassName={'pagination-next'}
            pageLinkClassName={'pagination-link'}
        />
    )
}

export default Pagination