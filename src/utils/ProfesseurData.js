import { FiList } from 'react-icons/fi'
import { BiArchiveIn } from 'react-icons/bi'

export const links = [
    {
        label: "Liste Cours",
        icon: FiList,
        to: '',
    },
    {
        label: "Cours Archivés",
        icon: BiArchiveIn,
        to: 'cours-archives',
    }
]