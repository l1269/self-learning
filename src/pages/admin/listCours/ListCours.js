import React, { useState, useEffect } from "react";
import { db } from "../../../firebase/config";
import { collection, onSnapshot, doc, updateDoc } from "firebase/firestore";
import "./ListCours.css";
import ModalEditCours from "../editCours/ModalEditCours";
import FiltreCours from "../../../components/filtreCours/FiltreCours";
import AfficheMesage from "../../../components/afficheMessage/AfficheMesage";
import Card from "../../../components/cards/Card";
import AjoutCoursModal from "./AjoutCoursModal";
import Button from "../../../components/button/Button";
import ModalCours from "../../../components/modalCours/ModalCours";
import { toast } from "react-toastify";
import SkeletonCours from "../../../components/skeleton/SkeletonCours";
import { useAuth } from "../../../hooks/useAuth";
import Pagination from "../../../components/pagination/Pagination";
import { FiExternalLink } from "react-icons/fi";
import { BsEye } from "react-icons/bs";


export default function ListCours() {
  const [showModal, setShowModal] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const [addCourseModal, setAddCourseModal] = useState(false);
  const [loading, setLoading] = useState(true);
  const [cours, setCours] = useState([]);
  const [savedCour, setSavedCour] = useState(null);
  const [listCours, setListCours] = useState([]);
  const coursCollectionRef = collection(db, "cours");
  const { toggleHeader, toggleHeaderRemove } = useAuth();
  const [disabled, setDisabled] = useState(false);

  // Gestion de pagination
  const [currentItems, setCurrentItems] = useState(null);
  const [pageCount, setPageCount] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);

  // Number cours perPage
  const number = 6

  const notification = () => toast("Cours archive");

  useEffect(() => {
    const getCours = async () => {
      // const q = query(coursCollectionRef, orderBy("timeStamp", "desc"));
      onSnapshot(coursCollectionRef, (querySnapshot) => {
        const usersInfo = [];
        querySnapshot.forEach((doc) => {
          usersInfo.push({ ...doc.data(), id: doc.id });
          setCours([...usersInfo]);
          setListCours([...usersInfo]);
        });
        setLoading(false);
      });
    };
    getCours();
  }, []);

  useEffect(() => {
    if (addCourseModal || openModal || showModal) {
      toggleHeader()
    }
    else {
      toggleHeaderRemove()
    }
  }, [addCourseModal, openModal, showModal]);


  // Pagination functions
  useEffect(() => {
    const endOffset = itemOffset + number;
    setCurrentItems(listCours?.filter((cour) => cour.isArchiver === false).slice(itemOffset, endOffset));
    setPageCount(Math.ceil(listCours?.filter((cour) => cour.isArchiver === false).length / number));
  }, [itemOffset, listCours, number]);

  const handlePageClick = (event) => {
    const newOffset = (event.selected * number) % listCours?.length;
    setItemOffset(newOffset);
  };

  const archiver = async (id) => {
    setDisabled(true);
    await updateDoc(doc(db, "cours", id), {
      isArchiver: true,
      archiverPar: "admin",
    });
    notification();
    setOpenModal(false);
    setDisabled(false);
  };

  const handleShow = () => {
    setOpenModal(false);
    setShowModal(true);
  };

  const handleCours = (cour) => {
    setSavedCour(cour);
    setOpenModal(true);
  };

  const archiveCours = () => {
    archiver(savedCour.id);
  };

  const showAddCoursModal = () => {
    setAddCourseModal(true);
  };

  const styles = {
    conatiner: {
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      marginBottom: 30,
      flexWrap: "wrap",
    }
  }

  return (
    <>
      <h4 className="mb-3 title">Liste des cours</h4>
      <div className="filter add-course"
        style={styles.conatiner}
      >
        {/* le filtrage de cours */}
        <FiltreCours
          cours={cours}
          setListCours={setListCours}
          setAddCourseModal={setAddCourseModal}
        />

        {/* Le button d'ajout de cours */}
        {listCours.filter((cour) => cour.isArchiver === false).length !== 0 && (
          <Button handleClick={showAddCoursModal} title={"nouveau cours"} />
        )}
      </div>
      {
        console.log('keur')
      }

      <div className="grid-container ">
        {!loading
          ? listCours?.filter((cour) => cour.isArchiver === false).length !==
          0 &&
          currentItems?.map((cour) => (
            <Card
              className="rmCard"
              key={cour.id}
              img={cour.img}
              titre={cour.titre}
              domaine={cour.domaine}
              duree={cour.date}
            >
              <button
                className="card-btn"
                style={{ marginRight: 10 }}
                onClick={() => handleCours(cour)}
              >
                <BsEye
                  style={{ fontWeight: 700, fontSize: 22 }}
                />
              </button>
              <a href={cour.url} target="_blank" rel="noreferrer" style={{ border: '1px solid #fff', borderRadius: 5, padding: '4px 5px' }}>
                <FiExternalLink color='#fff' size={22} />
              </a>
            </Card>
          ))
          : [1, 2, 3, 4, 5].map((n) => <SkeletonCours key={n} />)}

        {openModal && (
          <ModalCours
            setShowModal={setShowModal}
            cours={cours}
            onClose={setOpenModal}
            savedCour={savedCour}
          >
            <button className="modalBouttonEdit" onClick={handleShow}>
              Modifier
            </button>
            <button
              disabled={disabled}
              onClick={archiveCours}
              className="modalBoutonArchive"
            >
              Archiver
            </button>
          </ModalCours>
        )}
        {showModal && (
          <ModalEditCours
            cour={savedCour}
            onCloseModal={() => setShowModal(false)}
          />
        )}

        {addCourseModal && (
          <AjoutCoursModal
            setAddCourseModal={setAddCourseModal}
            addCourseModal={addCourseModal}
          />
        )}
      </div>


      {!loading &&
        listCours.filter((cour) => cour.isArchiver === false).length !== 0 && listCours?.filter((cour) => cour.isArchiver === false).length > number && (
          <Pagination pageCount={pageCount} handlePageClick={handlePageClick} />
        )}

      {!loading &&
        listCours.filter((cour) => cour.isArchiver === false).length === 0 && (
          <AfficheMesage
            message="Ajouter cours"
            error="Pas de cours pour le moment"
          >
            <button
              onClick={showAddCoursModal}
              className="btn-ajouter"
              style={{
                backgroundColor: "red",
                fontWeight: 500,
              }}
            >
              Ajouter cours
            </button>
          </AfficheMesage>
        )}
    </>
  );
}
