import './AdminSidebar.css'
import { FiList } from 'react-icons/fi'
import { AiOutlineFolderAdd } from 'react-icons/ai'
import { BiArchiveIn } from 'react-icons/bi'
import { CgUserList } from 'react-icons/cg'
import { AiOutlineUsergroupAdd } from 'react-icons/ai'
import { BsFileEarmarkPersonFill } from 'react-icons/bs'
import { BsFilePerson } from 'react-icons/bs'
import { Link } from 'react-router-dom'
import logo from '../../../assets/design/logo2.png'


export default function Sidebar() {

    return (
        <nav className='Sidebar vh-100 sticky-top p-1 p-md-3 d-sm-block d-none'>
            {/* logo */}
            <div className="logo p-lg-2 p-0">
                <div className="logo text-center text-lg-start mb-3 mt-0">
                    <img className='me-lg-1 logo-img' style={{ maxWidth: '40px' }} src={logo} alt="Logo" />
                    <h3 className=' mb-1 d-lg-inline d-none' style={{ fontWeight: '100', display: 'inline' }}>Learning</h3>
                </div>
            </div>
            {/* menu */}
            <ul className='p-0'>

                <li className='nav-item position-relative'>
                    <Link className='stretched-link' to=''>
                        <FiList className='icon' />
                        <span className='d-lg-inline d-none'>List Cours</span>
                    </Link>
                </li>

                <li className='nav-item position-relative'>
                    <Link className='stretched-link' to='ajout-cours'>
                        <AiOutlineFolderAdd className='icon' />
                        <span className='d-lg-inline d-none'>Ajouter Cours</span>
                    </Link>
                </li>

                <li className='nav-item position-relative'>
                    <Link className='stretched-link' to='cours-archives'>
                        <BiArchiveIn className='icon' />
                        <span className=' d-lg-inline d-none'>Cours Archivés</span>
                    </Link>
                </li>

                <li className='nav-item position-relative'>
                    <Link className='stretched-link' to='ajout-utilisateur'>
                        <AiOutlineUsergroupAdd className='icon' />
                        <span className=' d-lg-inline d-none'>Ajouter Utilisateur</span>
                    </Link>
                </li>

                <li className='nav-item position-relative'>
                    <Link className='stretched-link' to='list-profs'>
                        <CgUserList className='icon' />
                        <span className=' d-lg-inline d-none'>List Profs</span>
                    </Link>
                </li>

                <li className='nav-item position-relative'>
                    <Link className='stretched-link' to='profs-archives'>
                        <BsFileEarmarkPersonFill className='icon' />
                        <span className=' d-lg-inline d-none'>Profs Archivés</span>
                    </Link>
                </li>

                <li className='nav-item position-relative'>
                    <Link className='stretched-link' to='list-apprenants'>
                        <CgUserList className='icon' />
                        <span className=' d-lg-inline d-none'>List Apprenants</span>
                    </Link>
                </li>

                <li className='nav-item position-relative'>

                    <Link className='stretched-link' to='apprenants-archives'>
                        <BsFilePerson className='icon' />
                        <span className=' d-lg-inline d-none'>Apprenants Archivés</span>
                    </Link>

                </li>
                <li className='nav-item position-relative'>
                </li>
            </ul>
        </nav>
    )
}
