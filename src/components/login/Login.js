import React from 'react';
import { useState } from 'react';
import {useAuth} from '../../hooks/useAuth';
import {useNavigate} from 'react-router-dom'
import './Login.css'
import {HiOutlineMail} from 'react-icons/hi'
import {BiLockAlt} from 'react-icons/bi';
import logo from '../../assets/design/logo2.png'

const Login = () => {
  const navigate = useNavigate();

  const {login} = useAuth();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading ,setLoading] = useState(false)
  const [error, setError] = useState('');
  

  

  const handleSubmit = async (e) => {
    e.preventDefault();
        // vérifie si l'utilisateur est connecté à internet et renvoie un boolean
    if(!navigator.onLine) {
      return setError("Vérifier votre connexion internet");
    }
    if( email === '' || password === '' ){
      return setError('Veuillez remplir les champs vides');
    }
    if(password.length < 6) {    
      return setError('Mot de passe court (au moins 6 caracteres)');
    }

    try{
      setError('')
      setLoading(true)
      await login(email, password);
      navigate('/redirect')
    }
    catch(err) {
      setLoading(false)
      if(err.code === 'auth/invalid-email'){
        return setError(`L'adresse email est invalide !`)
      }
      if(err.code === 'auth/user-not-found'){
        return setError('Utilisateur introuvable !')
      }
      if(err.code === 'auth/wrong-password'){
        return setError('Email ou Mot de passe incorrecte')
      }
    }
    setLoading(false)
  }

  // useEffect(() => {
      
  // }, [])

  return (
    <>
      <div className="logo text-center d-flex justify-content-center mb-3 mt-4">
        <img className='me-1' style={{maxWidth: '40px'}} src={logo} alt="Logo" />
        <h3 className='align-self-end mb-1' style={{fontWeight: '100'}}>Learning</h3>
      </div>
      <p style={{fontWeight: '100'}} className='text-center'>Le meilleur endroit pour se former !</p>
      <div style={{maxWidth: '450px'}} className='container w-sm-75 mx-sm-auto shadow px-3 py-5'>
        <h1 className="text-center mb-4">Connexion</h1>
        {
          error && <div className='bg-danger p-4 text-light text-center mb-3'>{ error }</div>
        }
        <form style={{width: '100%'}} autoComplete='off' onSubmit={handleSubmit}>
          <div className="mb-3 position-relative">
            <HiOutlineMail className='subs-icon' />
            <input 
            placeholder='Email'
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            type="email" 
            className="login-input" 
            id="email" 
            aria-describedby="emailHelp" />
          </div>
          <div className="mb-3 position-relative">
            <BiLockAlt className='subs-icon' />
            <input 
            placeholder='Mot de passe'
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            type="password" 
            className="login-input" 
            id="password" />
          </div>
          <button disabled={loading} type="submit" className="btn py-2 auth w-100">
            {
              loading ? 'Connexion en cours...' : 'Connectez-vous'
            }  
          </button>
        </form>
      </div>
    </>
  )
}

export default Login